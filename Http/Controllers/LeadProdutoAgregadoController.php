<?php

namespace Modules\Client\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\FinanceiroCobranca;
use Modules\Client\Entities\FinanceiroContratacao;
use Modules\Client\Entities\FinanceiroFormaPagamento;
use Modules\Client\Entities\Lead;
use Modules\Client\Entities\LeadAgregado;
use Modules\Client\Entities\LeadCard;
use Modules\Client\Entities\LeadProdutoAgregado;
use Modules\Client\Entities\LeadProdutoAgregadoItens;
use Modules\Client\Entities\Produto;
use Modules\Client\Entities\Seguradora;
use Modules\Client\Entities\TermoCobranca;
use Modules\Client\Repositories\FinanceiroCobrancaRepository;
use Modules\Client\Repositories\LeadProdutoAgregadoRepository;
use Modules\Client\Repositories\FinanceiroContratacaoRepository;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Exception\ClientException;

class LeadProdutoAgregadoController extends Controller
{
    protected $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    //É a venda!! O inicio
    public function validaPedido($data){

        $repositoryLeadProdutoAgregado = new LeadProdutoAgregadoRepository();
        if(!isset($data['financeiro_contratacao']['data_vencimento']) || is_null($data['financeiro_contratacao']['data_vencimento']) || $data['financeiro_contratacao']['data_vencimento']=="") return ['message'=>'Preencha o campo financeiro_contratacao.data_vencimento'];
        unset($data['financeiro_contratacao']['data_vencimento']);
        $validateProdutoAgregado = $repositoryLeadProdutoAgregado
            ->builder()
            ->validate($data['lead_agregado_produto'],['lead_id']);
        if(is_array($validateProdutoAgregado)) return $validateProdutoAgregado;

        $repositoryFinanceiroContratacao = new FinanceiroContratacaoRepository();
        $validateFinanceiroContratacao = $repositoryFinanceiroContratacao
            ->builder()
            ->validate($data['financeiro_contratacao'],['lead_produto_agregados_id','valor_total','valor_mensal','vezes_cobranca','data_contratacao','usuario_id','data_importacao','fl_importacao','fl_ativo','no_remessa']);
        if(is_array($validateFinanceiroContratacao)) return $validateFinanceiroContratacao;

        if(!isset($data['lead_agregado_produto_itens']) || count($data['lead_agregado_produto_itens'])==0) return ['message'=>'Informe pelo menos um lead_agregado (lead_agregado_produto_itens[lead_agregado_id][])'];
        $resp = [];
        for($i=0;$i<count($data['lead_agregado_produto_itens']['lead_agregado_id']); $i++){
            $lead_agregado_id = $data['lead_agregado_produto_itens']['lead_agregado_id'][$i];
            $findProdutoAgregado = LeadProdutoAgregadoItens::where('lead_agregado_id',$lead_agregado_id)->get()->toArray();
            if(!empty($findProdutoAgregado)) {
                $resp[$i] = "Já existe um produto vendido para esse usuario (".$lead_agregado_id.")";
            } else {
                $find = LeadAgregado::where('lead_agregado_id',$lead_agregado_id)->first();
                if(is_null($find)) $resp[$i] = "Lead Agregado INVALIDO! (".$lead_agregado_id.")";
            }
        }
        if(!empty($resp)) return ['message'=>$resp];
        //valida cartão de credito
        $findLead = LeadAgregado::where('lead_agregado_id',$data['lead_agregado_produto_itens']['lead_agregado_id'][0])->first();
        $lead_id = $findLead->lead_id;
        $findLC = LeadCard::where('lead_id',$lead_id)->first();
        $erro_card = [];
        if($data['financeiro_contratacao']['financeiro_forma_pagamento_id']==2 && is_null($findLC)){
            if(!isset($data['cartao']['numero_cartao']) || $data['cartao']['numero_cartao']=="" || is_null($data['cartao']['numero_cartao'])) $erro_card[] = "Informar o numero do cartão (cartao.numero_cartao)";
            if(!isset($data['cartao']['nome_cartao']) || $data['cartao']['nome_cartao']=="" || is_null($data['cartao']['nome_cartao'])) $erro_card[] = "Informar o nome do titular cartão (cartao.nome_cartao)";
            if(!isset($data['cartao']['cvv']) || $data['cartao']['cvv']=="" || is_null($data['cartao']['cvv'])) $erro_card[] = "Informar o codigo de segurança do cartão (cartao.cvv)";
            if(!isset($data['cartao']['bandeira']) || $data['cartao']['bandeira']=="" || is_null($data['cartao']['bandeira'])) $erro_card[] = "Informar a bandeira do cartão (cartao.bandeira)";
            if(!isset($data['cartao']['mes']) || $data['cartao']['mes']=="" || is_null($data['cartao']['mes'])) $erro_card[] = "Informar o mes do cartão (cartao.mes)";
            if(!isset($data['cartao']['ano']) || $data['cartao']['ano']=="" || is_null($data['cartao']['ano'])) $erro_card[] = "Informar o ano do cartão (cartao.ano)";
            if(!empty($erro_card)) return ['message'=>$erro_card];
        }

        $findProduto = Produto::where('produto_id',$data['lead_agregado_produto']['produto_id'])->first();
        if(is_null($findProduto)) return ['message'=>'Produto passado não existe'];
        return true;
    }
    /***
     * Aqui é desencadeado a venda de um produto a inserção de suas ramificações
     */
    public function vendaProdutoPipeline(Request $request)
    {
        try {
            //DB::beginTransaction();
            $data = $request->all();
            $validate = $this->validaPedido($data);
            if(is_array($validate)) return response()->json($validate,400);
            $modelFormaPagamento = new FinanceiroFormaPagamento();
            $findFormaPagamento = $modelFormaPagamento
                ->where('financeiro_forma_pagamento_id',$data['financeiro_contratacao']['financeiro_forma_pagamento_id'])
                ->first();
            if(is_null($findFormaPagamento)) return [
                'message'=>['Não existe essa forma de pagamento']
            ];

            $modelLeadProdutoAgregado = new LeadProdutoAgregado();
            $modelFinanceiroContratacao = new FinanceiroContratacao();
            $modelFinanceiroCobranca = new FinanceiroCobranca();
            $modelLeadProdutoAgregadoItens = new LeadProdutoAgregadoItens();
            $modelLeadAgregado = new LeadAgregado();

            $findLead = $modelLeadAgregado->where('lead_agregado_id',$data['lead_agregado_produto_itens']['lead_agregado_id'][0])->first();
            $lead_id = $findLead->lead_id;
            //var_dump($lead_id); exit;
            $data['lead_agregado_produto']['lead_id'] = $lead_id;
            $data['financeiro_contratacao']['data_contratacao'] = date('Y-m-d H:i:s');
            $data['financeiro_contratacao']['usuario_id'] = $data['lead_agregado_produto']['usuario_id'];

            //save card credit
            $card = LeadCard::where('lead_id',$lead_id)->first();
            if(empty($data['cartao']) && $data['financeiro_contratacao']['financeiro_forma_pagamento_id']==2 && is_null($card)) {
                LeadCard::create([
                    'lead_id'=>$lead_id,
                    'lead_card_titular_nome'=>$data['cartao']['nome_cartao'],
                    'lead_card_cvv'=>$data['cartao']['cvv'],
                    'lead_card_numero'=>$data['cartao']['numero_cartao'],
                    'lead_card_mes'=>$data['cartao']['mes'],
                    'lead_card_ano'=>$data['cartao']['ano'],
                    'lead_card_bandeira'=>strtolower($data['cartao']['bandeira'])  ==  "mastercard" ? "Master" : ucfirst($data['cartao']['bandeira']),
                ]);
            }
            //
            $leadProdutoAgregadoID = $modelLeadProdutoAgregado->create($data['lead_agregado_produto'])->lead_produto_agregados_id;
            //
            $lead_agregado_itens = $data['lead_agregado_produto_itens']['lead_agregado_id'];

            for($i=0;$i<count($lead_agregado_itens); $i++){
                $modelLeadProdutoAgregadoItens->create([
                    'lead_produto_agregados_id'=>$leadProdutoAgregadoID,
                    'lead_agregado_id'=>$lead_agregado_itens[$i]
                ]);
            }
            $modelProduto =  new Produto();
            $modelTermo =  new TermoCobranca();
            $findProduto = $modelProduto->find($data['lead_agregado_produto']['produto_id']);

            $data['financeiro_contratacao']['lead_produto_agregados_id'] = $leadProdutoAgregadoID;
            $data['financeiro_contratacao']['valor_total'] = count($lead_agregado_itens)*$findProduto->produto_valor*$findProduto->produto_parcelas;
            $data['financeiro_contratacao']['valor_mensal'] = count($lead_agregado_itens)*$findProduto->produto_valor;
            $data['financeiro_contratacao']['vezes_cobranca'] = $findProduto->produto_parcelas;
            $financeiroContratoID = $modelFinanceiroContratacao->create($data['financeiro_contratacao'])->financeiro_contratacao_id;

            $data_save = [
                'data_inclusao'=>date('Y-m-d H:i:s'),
                'data_vencimento'=>$data['financeiro_contratacao']['data_vencimento'],
                'valor'=>$data['financeiro_contratacao']['valor_mensal'],
                'financeiro_forma_pagamento_id'=>$data['financeiro_contratacao']['financeiro_forma_pagamento_id'],
                'financeiro_contratacao_id'=>$financeiroContratoID,
                'usuario_id'=>$data['financeiro_contratacao']['usuario_id'],
                'gateway_slug'=>$findFormaPagamento->financeiro_forma_pagamento_nome."|".$findFormaPagamento->financeiro_forma_pagamento_gateway,
                'fl_primeira_parcela'=>1
            ];
            $financeiro_cobranca_id = $modelFinanceiroCobranca->create($data_save)->financeiro_cobranca_id;

            $modelTermo->create([
                //5 dias
                'termo_cobranca_token'=>base64_encode(time()+(5*(86400))),
                'financeiro_contratacao_id'=>$financeiroContratoID
            ]);

            if($data['financeiro_contratacao']['financeiro_forma_pagamento_id']==2 ){
                //return $card;
                $url = 'http://pag.agentebrasil.com/pagamentos/generateCobCardCredit';
                //$cartao = $data[0]['lead_produto_agregado']['lead']['cartao_credito'][0];
                $req =  $this->client->request('POST',$url,[
                    //'verify'=>false,
                    'form_params'=>[
                        'financeiro_cobranca_id'=>$financeiro_cobranca_id,
                        'Payment'=>[
                            'CreditCard'=>[
                                'CardNumber'=>$card->lead_card_numero,
                                'Holder'=>$card->lead_card_titular_nome,
                                'ExpirationDate'=>$card->lead_card_mes."/".$card->lead_card_ano,
                                'Brand'=>strtolower($card->lead_card_bandeira) ==  "mastercard" ? "Master" : ucfirst($card->lead_card_bandeira),
                                'SecurityCode'=>$card->lead_card_cvv
                            ]
                        ]
                    ]
                ]);
                $jsonR = json_decode($req->getBody()->getContents());
                if($jsonR->status=="pago"){
                    TermoCobranca::where('financeiro_contratacao_id',$financeiroContratoID)->update([
                        'termo_cobranca_termo_ok'=>1,
                        'termo_cobranca_termo_ok_date'=>date('Y-m-d H:i:s'),
                        'termo_cobranca_last_hit'=>date('Y-m-d H:i:s')
                    ]);
                } else {
                    Lead::where('lead_id',$lead_id)->update(['lead_tag'=>'problema_cc']);
                }
                return response()->json(['data'=>$jsonR,'type'=>'cartao_credito'],200);
            } else {
                $this->sendEmail($financeiroContratoID,$lead_id,$findProduto->seguradora_id);
                $this->sendSms($financeiroContratoID,$lead_id,$findProduto->seguradora_id);
            }

            //DB::commit();
            return response()->json(['message'=>'Salvo com sucesso'],200);
        } catch (ClientException $e){
            //DB::rollback();
            $response = $e->getResponse();
            $req = $response->getBody()->getContents();
            return response()->json(['message'=>$req],400);
        }
    }

    public function vendaProdutoPipeline2(Request $request)
    {
        //return $request->all();
        //try {
        //DB::beginTransaction();
        $data = $request->all();
        $validate = $this->validaPedido($data);
        if(is_array($validate)) return response()->json($validate,400);
        $modelFormaPagamento = new FinanceiroFormaPagamento();
        $findFormaPagamento = $modelFormaPagamento
            ->where('financeiro_forma_pagamento_id',$data['financeiro_contratacao']['financeiro_forma_pagamento_id'])
            ->first();
        if(is_null($findFormaPagamento)) return [
            'message'=>['Não existe essa forma de pagamento']
        ];
        //var_dump($validate); exit;
        $modelLeadProdutoAgregado = new LeadProdutoAgregado();
        $modelFinanceiroContratacao = new FinanceiroContratacao();
        $modelFinanceiroCobranca = new FinanceiroCobranca();
        $modelLeadProdutoAgregadoItens = new LeadProdutoAgregadoItens();
        $modelLeadAgregado = new LeadAgregado();

        $findLead = $modelLeadAgregado->where('lead_agregado_id',$data['lead_agregado_produto_itens']['lead_agregado_id'][0])->first();
        $lead_id = $findLead->lead_id;
        //var_dump($data); exit;
        $data['lead_agregado_produto']['lead_id'] = $lead_id;
        $data['financeiro_contratacao']['data_contratacao'] = date('Y-m-d H:i:s');
        $data['financeiro_contratacao']['usuario_id'] = $data['lead_agregado_produto']['usuario_id'];
        //var_dump($data); exit;
        //save card credit
        $card = LeadCard::where('lead_id',$lead_id)->first();
        //var_dump($card); exit;
        if(empty($data['cartao']) && $data['financeiro_contratacao']['financeiro_forma_pagamento_id']==2 && is_null($card)) {
            LeadCard::create([
                'lead_id'=>$lead_id,
                'lead_card_titular_nome'=>$data['cartao']['nome_cartao'],
                'lead_card_cvv'=>$data['cartao']['cvv'],
                'lead_card_numero'=>$data['cartao']['numero_cartao'],
                'lead_card_mes'=>$data['cartao']['mes'],
                'lead_card_ano'=>$data['cartao']['ano'],
                'lead_card_bandeira'=>strtolower($data['cartao']['bandeira'])  ==  "mastercard" ? "Master" : ucfirst($data['cartao']['bandeira']),
            ]);
        }
        //
        $leadProdutoAgregadoID = $modelLeadProdutoAgregado->create($data['lead_agregado_produto'])->lead_produto_agregados_id;
        var_dump($leadProdutoAgregadoID); exit;
        //
        $lead_agregado_itens = $data['lead_agregado_produto_itens']['lead_agregado_id'];

        for($i=0;$i<count($lead_agregado_itens); $i++){
            $modelLeadProdutoAgregadoItens->create([
                'lead_produto_agregados_id'=>$leadProdutoAgregadoID,
                'lead_agregado_id'=>$lead_agregado_itens[$i]
            ]);
        }
        $modelProduto =  new Produto();
        $modelTermo =  new TermoCobranca();
        $findProduto = $modelProduto->find($data['lead_agregado_produto']['produto_id']);

        $data['financeiro_contratacao']['lead_produto_agregados_id'] = $leadProdutoAgregadoID;
        $data['financeiro_contratacao']['valor_total'] = count($lead_agregado_itens)*$findProduto->produto_valor*$findProduto->produto_parcelas;
        $data['financeiro_contratacao']['valor_mensal'] = count($lead_agregado_itens)*$findProduto->produto_valor;
        $data['financeiro_contratacao']['vezes_cobranca'] = $findProduto->produto_parcelas;
        $financeiroContratoID = $modelFinanceiroContratacao->create($data['financeiro_contratacao'])->financeiro_contratacao_id;

        $data_save = [
            'data_inclusao'=>date('Y-m-d H:i:s'),
            'data_vencimento'=>$data['financeiro_contratacao']['data_vencimento'],
            'valor'=>$data['financeiro_contratacao']['valor_mensal'],
            'financeiro_forma_pagamento_id'=>$data['financeiro_contratacao']['financeiro_forma_pagamento_id'],
            'financeiro_contratacao_id'=>$financeiroContratoID,
            'usuario_id'=>$data['financeiro_contratacao']['usuario_id'],
            'gateway_slug'=>$findFormaPagamento->financeiro_forma_pagamento_nome."|".$findFormaPagamento->financeiro_forma_pagamento_gateway,
            'fl_primeira_parcela'=>1
        ];
        $financeiro_cobranca_id = $modelFinanceiroCobranca->create($data_save)->financeiro_cobranca_id;

        $modelTermo->create([
            //5 dias
            'termo_cobranca_token'=>base64_encode(time()+(5*(86400))),
            'financeiro_contratacao_id'=>$financeiroContratoID
        ]);

        if($data['financeiro_contratacao']['financeiro_forma_pagamento_id']==2 ){
            //return $card;
            $url = 'http://pag.agentebrasil.com/pagamentos/generateCobCardCredit';
            //$cartao = $data[0]['lead_produto_agregado']['lead']['cartao_credito'][0];
            $req =  $this->client->request('POST',$url,[
                //'verify'=>false,
                'form_params'=>[
                    'financeiro_cobranca_id'=>$financeiro_cobranca_id,
                    'Payment'=>[
                        'CreditCard'=>[
                            'CardNumber'=>$card->lead_card_numero,
                            'Holder'=>$card->lead_card_titular_nome,
                            'ExpirationDate'=>$card->lead_card_mes."/".$card->lead_card_ano,
                            'Brand'=>strtolower($card->lead_card_bandeira) ==  "mastercard" ? "Master" : ucfirst($card->lead_card_bandeira),
                            'SecurityCode'=>$card->lead_card_cvv
                        ]
                    ]
                ]
            ]);
            $jsonR = json_decode($req->getBody()->getContents());
            if($jsonR->status=="pago"){
                TermoCobranca::where('financeiro_contratacao_id',$financeiroContratoID)->update([
                    'termo_cobranca_termo_ok'=>1,
                    'termo_cobranca_termo_ok_date'=>date('Y-m-d H:i:s'),
                    'termo_cobranca_last_hit'=>date('Y-m-d H:i:s')
                ]);
            } else {
                Lead::where('lead_id',$lead_id)->update(['lead_tag'=>'problema_cc']);
            }
            return response()->json(['data'=>$jsonR,'type'=>'cartao_credito'],200);
        } else {
            $this->sendEmail($financeiroContratoID,$lead_id,$findProduto->seguradora_id);
            $this->sendSms($financeiroContratoID,$lead_id,$findProduto->seguradora_id);
        }

        //DB::commit();
        return response()->json(['message'=>'Salvo com sucesso'],200);
//        } catch (ClientException $e){
//            //DB::rollback();
//            $response = $e->getResponse();
//            $req = $response->getBody()->getContents();
//            return response()->json(['message'=>$req],400);
//        }
    }

    public function sendEmail($contratacao_id,$lead_id,$seguradora_id)
    {
        $url = getenv("SERVICO_CLIENT")."/client/termos-de-aceite/".$contratacao_id;
        $lead = Lead::where('lead_id',$lead_id)->first();
        $seguradora = Seguradora::where('seguradora_id',$seguradora_id)->first();
        $message = "Olá ".$lead->lead_nome."<br> Segue o link para confirmação e informações <a href=".$url.">".$url."</a>";

        $this->client->request('post','http://contact.agentebrasil.com/contact/contact/store',[
            'form_params'=>[
                'canal_id'=>1,
                'destinatario_email'=>$lead->lead_email,
                'destinatario_fone'=>$lead->lead_fone,
                'destinatario_nome'=>$lead->lead_nome,
                'lead_id'=>$lead_id,
                'mensagem'=>$message,
                'produto_nome_email'=>'Agente Brasil',
                'remetente_email'=>'noreply@agentebrasil.com.br',
                'remetente_nome'=>$seguradora->seguradora_nome,
                'subject'=>'Confirme seus dados'
            ]
        ]);
    }

    public function sendSms($contratacao_id,$lead_id,$seguradora_id)
    {
        $lead = Lead::where('lead_id',$lead_id)->first();
        $seguradora = Seguradora::where('seguradora_id',$seguradora_id)->first();
        $message = "AB DENTAL > Bem-vindo! Gere seu boleto clicando no link: ".
            getenv("SERVICO_CLIENT")."/client/termos-de-aceite/".$contratacao_id;
        $this->client->request('post','http://contact.agentebrasil.com/contact/contact/store',[
            'form_params'=>[
                'canal_id'=>2,
                'destinatario_fone'=>"55".$lead->lead_fone,
                'destinatario_nome'=>$lead->lead_nome,
                'lead_id'=>$lead_id,
                'mensagem'=>$message,
                'produto_nome_email'=>'Agente Brasil',
                'remetente_nome'=>$seguradora->seguradora_nome,
                'subject'=>'Confirme seus dados'
            ]
        ]);
    }

    public function getLeadAgregadoContratacao($lead_agregado_id)
    {
        $modelLeadAgregado = new LeadProdutoAgregado();
        $findLeadProdutoAgregado = $modelLeadAgregado
            ->with([
                'FinanceiroContratacao'=>function($query){
                    $query->with('FinanceiroCobranca');
                },
                'Produto'
            ])
            ->where('lead_agregados_id',$lead_agregado_id)
            ->get();
        return $findLeadProdutoAgregado;
    }

    public function gerarCobrancas($financeiroContratoID,$data_financeiro,$data_primeiro_pagamento)
    {
        $repositoryFinanceiroCobranca = new FinanceiroCobrancaRepository();
        $data_save = [
            'data_inclusao'=>date('Y-m-d H:i:s'),
            'data_vencimento'=>$data_primeiro_pagamento,
            'valor'=>$data_financeiro['valor_mensal'],
            'financeiro_forma_pagamento_id'=>$data_financeiro['financeiro_forma_pagamento_id'],
            'financeiro_contratacao_id'=>$financeiroContratoID,
            'usuario_id'=>$data_financeiro['usuario_id']
        ];
        $store = $repositoryFinanceiroCobranca->store($data_save);
    }
    //retorna item paginados
    public function index()
    {
        $repository = new LeadProdutoAgregadoRepository;
        return $repository->builder()->paginate();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new LeadProdutoAgregadoRepository;
        $save = $repository->store($data,['carteira_id']);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new LeadProdutoAgregadoRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new LeadProdutoAgregadoRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

}