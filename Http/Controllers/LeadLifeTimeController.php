<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Repositories\LeadLifeTimeRepository;

class LeadLifeTimeController extends Controller
{
    
    //retorna item paginados
    public function index($lead_id = null)
    {
        $repository = new LeadLifeTimeRepository;
        $builder = $repository->builder()->with([
            'OperacaoTimeLifeLead'=>function($query){
                $query->select(['lead_life_time_operacao_id','lead_life_time_operacao_name']);
            },
            'Usuario'=>function($query){
                $query->select(['usuario_id','login']);
            }
        ])->where('lead_id',$lead_id)->get();
        return $builder;
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new LeadLifeTimeRepository;
        $save = $repository->store($data);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new LeadLifeTimeRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new LeadLifeTimeRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

}