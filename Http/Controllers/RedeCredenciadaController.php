<?php

namespace Modules\Client\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\Campanha;
use Modules\Client\Entities\LogRedecredenciada;
use Modules\Client\Entities\RedeCredenciada;
use Modules\Client\Repositories\RedeCredenciadaRepository;

class RedeCredenciadaController extends Controller
{
    protected $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    public function importRedeCredenciada(Request $request)
    {
        $sanetizeFields = function($value){
            $new = str_replace('"','',$value);
            return $new;
        };
        try {
            $data = $request->all();
            if (!isset($data['csv_import']) || empty($data['csv_import'])) return response()->json(['message' => 'Enviar um csv de importacao'], 400);
            if (!isset($data['seguradora_id']) || empty($data['seguradora_id'])) return response()->json(['message' => 'seguradora_id está null ou vazio'], 400);

            $filename = date('Y-m-d') . '_' . time() . '.csv';
            $req = $request->file('csv_import')->move(storage_path('csvs/'), $filename);
            $getCsv = file_get_contents(storage_path("csvs/") . $filename);
            $linhas = explode("\n", $getCsv);
            $date_importacao = date('Y-m-d H:i:s');
            RedeCredenciada::where("seguradora_id",$data['seguradora_id'])->update(['fl_ativo'=>0]);
            foreach ($linhas as $linha) {
                $colum = explode(";", $linha);
                if(count($colum) != 2) continue;
                RedeCredenciada::create([
                    'data_importacao' => $date_importacao,
                    'uf' => $sanetizeFields($colum[0]),
                    'cidade' => $sanetizeFields($colum[1]),
                    'seguradora_id' => $data['seguradora_id'],
                    'codeimport' => $filename
                ]);
            }
            return response()->json(['message'=>'Rede credenciada importada com sucesso']);
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()]);
        }
    }
    //retorna item paginados
    public function index($seguradora_id=null,$estado=null,$cidade=null,$ip=null)
    {
        $repository = new RedeCredenciadaRepository;

        if(!is_null($ip)){
            $request = $this->client->request('get','http://ip-api.com/json/'.$ip);
            $data = json_decode($request->getBody()->getContents());
            $estado = $data->region;
            $cidade = $data->city;
            return $repository->builder()
                ->with('Seguradora')
                ->where('uf','like','%'.$estado.'%')
                ->where('cidade','like','%'.$cidade.'%')
                ->where('seguradora_id',$seguradora_id)
                ->orderBy('fl_ativo','desc')
                ->get();
        } else if(!is_null($seguradora_id) && !is_null($estado) && !is_null($cidade)){
            return $repository->builder()
                ->with('Seguradora')
                ->where('uf','like','%'.$estado.'%')
                ->where('cidade','like','%'.$cidade.'%')
                ->where('seguradora_id',$seguradora_id)
                ->orderBy('fl_ativo','desc')
                ->get();
        }
        return $repository->builder()
            ->with('Seguradora')
            ->limit(100)
            ->orderBy('fl_ativo','desc')
            ->get();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new RedeCredenciadaRepository;
        $save = $repository->store($data,['cep_final','cep_inicial','codeimport','data_importacao','fl_ativo']);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new RedeCredenciadaRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new RedeCredenciadaRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

    public function getLeadRedeCredenciada($estado,$cidade,$campanha_id,$lead_id)
    {
        $getObjSeguradora = $this->getSeguradora($campanha_id);
        if(empty($getObjSeguradora)) return response()->json(['message'=>'Campanha não existe'],400);
        $findRede = RedeCredenciada::where('uf','like','%'.strtoupper($estado).'%')->where('cidade','like','%'.strtoupper($cidade).'%')->first();
        if(is_null($findRede)) {
            LogRedecredenciada::create([
                'data_consulta'=>date('Y-m-d H:i:s'),
                'uf'=>strtoupper($estado),
                'cidade'=>strtoupper($cidade),
                'produto_id'=>$getObjSeguradora->produto->produto_id,
                'campanha_id'=>$campanha_id,
                'fl_redecredenciada_valid'=>0,
                'lead_id'=>$lead_id
            ]);

            return response()->json(['message'=>'Não existe cobertura para esta cidade!'],400);
        }
        //caso exista
        LogRedecredenciada::create([
            'data_consulta'=>date('Y-m-d H:i:s'),
            'uf'=>strtoupper($estado),
            'cidade'=>strtoupper($cidade),
            'produto_id'=>$getObjSeguradora->produto->produto_id,
            'campanha_id'=>$campanha_id,
            'fl_redecredenciada_valid'=>1,
            'lead_id'=>$lead_id
        ]);
        return response()->json(['message'=>'Existe rede credenciada para essa cidade!'],200);
    }

    public function getSeguradora($campanha_id)
    {
        $data = Campanha::with([
            'Produto'=>function($query){
                $query->select(['produto_id','produto_nome','seguradora_id'])->with([
                    'Seguradora'=>function($query){
                        $query->select('seguradora_id','seguradora_nome');
                    }
                ]);
            }
        ])->where('campanha_id',$campanha_id)->first();

        return $data;
    }

    public function getEstadoCidadeByIp($ip,$campanha_id,$lead_id)
    {
        try {
            $request = $this->client->request('get','http://ip-api.com/json/'.$ip);
            $data = json_decode($request->getBody()->getContents());
            $estado = $data->region;
            $cidade = $data->city;
            return $this->getLeadRedeCredenciada($estado,$cidade,$campanha_id,$lead_id);
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }
}