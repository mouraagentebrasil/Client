<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 11/01/2017
 * Time: 17:28
 */

namespace Modules\Client\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\FinanceiroCobranca;
use Modules\Client\Entities\FinanceiroContratacao;
use Modules\Client\Entities\Lead;
use Modules\Client\Entities\LeadAgregado;
use Modules\Client\Entities\Seguradora;
use Modules\Client\Entities\TermoCobranca;
use Modules\Client\Events\EventAlterRegisterLead;
use Modules\Client\Repositories\LeadRepository;

class TermosAceiteClienteController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function index($contracao = null)
    {
        $finCob = FinanceiroCobranca::where('financeiro_contratacao_id',$contracao)->first();
        if(is_null($finCob)){
            return "Contratação não encontrada!";
        }
        $finCob = FinanceiroContratacao::with([
            'TermoCobranca',
            'LeadProdutoAgregado'=>function($query){
                $query->with([
                    'Lead',
                    'Produto'=>function($query){
                        $query->with(['Seguradora']);
                    },
                    'LeadProdutoAgregadoItens'=>function($query){
                        $query->with(['LeadAgregado']);
                    }
                ]);
            }
        ])
            ->where('financeiro_contratacao_id',$contracao)->first();
        $data = $finCob->toArray();
        //return $data;
        if($data['termo_cobranca']['termo_cobranca_termo_ok']==1){
            TermoCobranca::where('financeiro_contratacao_id',$data['financeiro_contratacao_id'])->update([
                'termo_cobranca_last_hit'=>date('Y-m-d H:i:s')
            ]);
        }
        TermoCobranca::where('financeiro_contratacao_id',$data['financeiro_contratacao_id'])->increment('number_hits');
        return view("client::termos_aceite_cliente",['finCob'=>$finCob->toArray()]);
    }

    public function updateLeadTermoCobranca(Request $request)
    {
        $data = $request->all();
        $financeiro_contratacao_id = $data['financeiro_contratacao_id'];
        $termo = $this->checkTermo($financeiro_contratacao_id);
        if(is_array($termo)) return response()->json($termo,401);

        $data['usuario_id'] = null;
        unset($data['financeiro_contratacao_id']);
        $data['lead_nascimento'] = date('Y-m-d',strtotime($data['lead_nascimento']));
        $repository = new LeadRepository();
        $id = $data['lead_id'];
        $dataOld = $repository->builder()->find($id);

        $update = $repository->update($id,$data,'*');
        //testa se o respositorio retorna um erro,
        if(is_array($update)) return response()->json($update,400);
        //evento de registro de alteração de lead
        $data['lead_id'] = $id;
        event(new EventAlterRegisterLead($dataOld,$data));
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }

    public function updateLeadAgregadoTermoCobranca(Request $request)
    {
        $data = $request->all();
        $financeiro_contratacao_id = $data[0]['financeiro_contratacao_id'];
        $termo = $this->checkTermo($financeiro_contratacao_id);
        if(is_array($termo)) return response()->json($termo,401);
        try {
            DB::beginTransaction();
            foreach ($data as $la){
                unset($la['financeiro_contratacao_id']);
                $la['lead_agregado_nascimento'] = date("Y-m-d",strtotime($la['lead_agregado_nascimento']));
                LeadAgregado::where('lead_agregado_id',$la['lead_agregado_id'])->update($la);
            }
            DB::commit();
            return ['message'=>'Atualizado com sucesso'];
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }

    public function confirmaTermoCobranca($financeiro_contratacao_id)
    {
        $termo = $this->checkTermo($financeiro_contratacao_id);
        if(is_array($termo)) return response()->json($termo,401);
        try {

            //a partir da forma de pagamento cria uma requisição de cobranca no pag
            $data = $this->getContratacao($financeiro_contratacao_id);
            //return response()->json($data[0]['lead_produto_agregado']['lead']['cartao_credito'][0],400);
            //verifica a forma de pagamento
            if($data[0]['financeiro_forma_pagamento_id']==1) {
                //boleto
                $url = "http://pag.agentebrasil.com/pagamentos/generateBoletoFinanceiroAB/".$data[0]['financeiro_cobranca'][0]['financeiro_cobranca_id'];
                $request = $this->client->request('GET',$url);
                $jsonR = json_decode($request->getBody()->getContents());
                $this->sendEmailBoleto(
                    $data[0]['lead_produto_agregado']['lead_id'],
                    $data[0]['lead_produto_agregado']['produto']['seguradora']['seguradora_id'],
                    $jsonR
                );

                $this->sendSmsBoleto(
                    $data[0]['lead_produto_agregado']['lead_id'],
                    $data[0]['lead_produto_agregado']['produto']['seguradora']['seguradora_id'],
                    $jsonR
                );
                //atualiza o termo
                TermoCobranca::where('financeiro_contratacao_id',$financeiro_contratacao_id)->update([
                    'termo_cobranca_termo_ok'=>1,
                    'termo_cobranca_termo_ok_date'=>date('Y-m-d H:i:s'),
                    'termo_cobranca_last_hit'=>date('Y-m-d H:i:s')
                ]);
                //
                return response()->json(['data'=>$jsonR,'type'=>'boleto'],200);
            } elseif($data[0]['financeiro_forma_pagamento_id']==2){
                //cartao
                $url = getenv('SERVICO_PAGAMENTO').'/pagamentos/generateCobCardCredit';
                $cartao = $data[0]['lead_produto_agregado']['lead']['cartao_credito'][0];
                $request =  $this->client->request('POST',$url,[
                    'form_params'=>[
                        'financeiro_cobranca_id'=>$data[0]['financeiro_cobranca'][0]['financeiro_cobranca_id'],
                        'Payment'=>[
                            'CreditCard'=>[
                                'CardNumber'=>$cartao['lead_card_numero'],
                                'Holder'=>$cartao['lead_card_titular_nome'],
                                'ExpirationDate'=>$cartao['lead_card_mes']."/".$cartao['lead_card_ano'],
                                'Brand'=>ucfirst($cartao['lead_card_bandeira']),
                                'SecurityCode'=>$cartao['lead_card_cvv'],
                            ]
                        ]
                    ]
                ]);
                $jsonR = json_decode($request->getBody()->getContents());
                if($jsonR->status=="pago"){
                    TermoCobranca::where('financeiro_contratacao_id',$financeiro_contratacao_id)->update([
                        'termo_cobranca_termo_ok'=>1,
                        'termo_cobranca_termo_ok_date'=>date('Y-m-d H:i:s'),
                        'termo_cobranca_last_hit'=>date('Y-m-d H:i:s')
                    ]);
                }
                return response()->json(['data'=>$jsonR,'type'=>'cartao_credito'],200);
            }
            //
            //enviar a forma de pagamento para ayty
            return response()->json(['message'=>'Confirmado com sucesso!'],200);
        } catch (\Exception $e){
            return response()->json(['message'=>'Erro: '.$e->getMessage()],400);
        }
    }

    public function checkTermo($financeiro_contratacao_id)
    {
        $termo = TermoCobranca::where('financeiro_contratacao_id',$financeiro_contratacao_id)->first();
        if(is_null($termo)) return ['message'=>'Contratacao não encontrada'];
        if($termo->termo_cobranca_termo_ok == 1) return ['message'=>'Termo já confirmado'];
        $now = time();
        if($now > (base64_decode($termo->termo_cobranca_token))) return ['message'=>'Termo de contratação expirada!'];
        return null;
    }

    public function getContratacao($contracao) {
        return FinanceiroContratacao::with([
            'TermoCobranca',
            'LeadProdutoAgregado'=>function($query){
                $query->with([
                    'Lead'=>function($query){
                        $query->with(['CartaoCredito']);
                    },
                    'Produto'=>function($query){
                        $query->with(['Seguradora']);
                    },
                    'LeadProdutoAgregadoItens'=>function($query){
                        $query->with(['LeadAgregado']);
                    },
                ]);
            },
            'FinanceiroCobranca'=>function($query){
                $query->orderBy('financeiro_cobranca_id','asc')->limit(1);
            }
        ])
            ->where('financeiro_contratacao_id',$contracao)->get()->toArray();

    }

    public function sendEmailBoleto($lead_id,$seguradora_id,$infoBol)
    {
        $lead = Lead::where('lead_id',$lead_id)->first();
        $seguradora = Seguradora::where('seguradora_id',$seguradora_id)->first();
        $message = "Olá ".$lead->lead_nome."!<br> Clique no link para a visualização do boleto com vencimento  para ".
            date('d/m/Y',strtotime($infoBol->vencimento))." no valor de R$ ".$infoBol->valor_fatura." <br><a href=".$infoBol->file.">".$infoBol->file."</a>".
            " ou pague com a linha digitavel ".$infoBol->linha_digitavel;

        $this->client->request('post','http://contact.agentebrasil.com/contact/contact/store',[
            'form_params'=>[
                'canal_id'=>1,
                'destinatario_email'=>$lead->lead_email,
                'destinatario_fone'=>$lead->lead_fone,
                'destinatario_nome'=>$lead->lead_nome,
                'lead_id'=>$lead_id,
                'mensagem'=>$message,
                'produto_nome_email'=>'Agente Brasil',
                'remetente_email'=>'noreply@agentebrasil.com.br',
                'remetente_nome'=>$seguradora->seguradora_nome,
                'subject'=>'Primeiro Boleto '.$seguradora->seguradora_nome
            ]
        ]);
    }

    public function sendSmsBoleto($lead_id,$seguradora_id,$infoBol)
    {
        $lead = Lead::where('lead_id',$lead_id)->first();
        $seguradora = Seguradora::where('seguradora_id',$seguradora_id)->first();
        $message = "AB DENTAL > clique e acesse seu boleto: ".$infoBol->file;

        $this->client->request('post','http://contact.agentebrasil.com/contact/contact/store',[
            'form_params'=>[
                'canal_id'=>2,
                'destinatario_fone'=>"55".$lead->lead_fone,
                'destinatario_nome'=>$lead->lead_nome,
                'lead_id'=>$lead_id,
                'mensagem'=>$message,
                'produto_nome_email'=>'Agente Brasil',
                'remetente_nome'=>$seguradora->seguradora_nome,
                'subject'=>'Primeiro Boleto '.$seguradora->seguradora_nome
            ]
        ]);
    }

    public function updateFormaPagamentoBoleto($financeiro_contratacao_id)
    {
        try {
            FinanceiroContratacao::find($financeiro_contratacao_id)->update([
                'financeiro_forma_pagamento_id'=>1
            ]);
            FinanceiroCobranca::where('financeiro_contratacao_id',$financeiro_contratacao_id)->update([
                'gateway_slug'=>'Boleto|AgenteBrasil',
                'financeiro_forma_pagamento_id'=>1
            ]);
            return $this->confirmaTermoCobranca($financeiro_contratacao_id);
        } catch (\Exception $e) {
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }
}