<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\FinanceiroContratacao;
use Modules\Client\Entities\LeadProdutoAgregado;
use Modules\Client\Repositories\FinanceiroContratacaoRepository;

class FinanceiroContratacaoController extends Controller
{
    
    //retorna item paginados
    public function index()
    {
        $repository = new FinanceiroContratacaoRepository;
        return $repository->builder()->paginate();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new FinanceiroContratacaoRepository;
        $save = $repository->store($data);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new FinanceiroContratacaoRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new FinanceiroContratacaoRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

    public function getContratacao($lead_id){
        $finCob = LeadProdutoAgregado::with([
            'Produto'=>function($query){
                $query->select('produto_id','produto_nome');
            },
            'Usuario'=>function($query){
                $query->select('usuario_id','login');
            },
            'LeadProdutoAgregadoItens'=>function($query){
                $query->with(['LeadAgregado'=>function($query){
                    $query->select(['lead_agregado_id','lead_agregado_nome']);
                }]);
            },
            'FinanceiroContratacao'=>function($query){
                $query->with('FinanceiroCobranca');
            }])->where('lead_id',$lead_id);
        return $finCob->first();
    }
}