<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\LeadAgregado;
use Modules\Client\Events\EventAlterLeadAgregado;
use Modules\Client\Events\EventAlterRegisterLead;
use Modules\Client\Events\EventRegisterLeadAgregado;
use Modules\Client\Repositories\LeadAgregadoRepository;

class LeadAgregadoController extends Controller
{
    //impedir a multititularidade
    public function checkLeadTitular($lead_id){
        $find = LeadAgregado::where('lead_id',$lead_id)->whereIn('lead_agregado_tipo',['titular','titular_pagador'])->where('fl_ativo',1)->first();
        if(is_null($find)) return false;
        return true;
    }
    //retorna item paginados
    public function index($buscar = null)
    {
        $repository = new LeadAgregadoRepository;

        $builder = $repository->builder()->with(['lead'=>function($query) use ($buscar) {
            $query->select('lead_id','lead_nome');
        }]);
        $builder->where('lead_agregado_id',$buscar)
            ->orWhere('lead_agregado_nome','like','%'.$buscar.'%');
        return $builder->paginate();
        //return $repository->builder()->paginate();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {

        $data = $request->all();
        $dataOld = $data;
        unset($data['usuario_id']);

        $repository = new LeadAgregadoRepository;
        $data['lead_agregado_criacao'] = date('Y-m-d H:i:s');

        if($data['lead_agregado_tipo']=='titular' && $this->checkLeadTitular($data['lead_id'])) {
            return response()->json(['message'=>['Não pode ser salvo pois já existe um titular']]);
        }

        $save = $repository->store($data,['lead_agregado_complemento','lead_agregado_email']);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        event(new EventRegisterLeadAgregado($data));
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $usuario_id = $data['usuario_id'];
        unset($data['usuario_id']);

        $repository = new LeadAgregadoRepository;
        $dataOld = $repository->builder()->find($id)->toArray();

        if($data['lead_agregado_tipo']=='titular' && $this->checkLeadTitular($data['lead_id'])==true) {
            return response()->json(['message'=>['Não pode ser atualizado pois já existe um titular']]);
        }
        $update = $repository->update($id,$data,['lead_agregado_criacao','lead_agregado_complemento','lead_agregado_parentesco','lead_agregado_civil']);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);

        $data['usuario_id'] = $usuario_id;
        event(new EventAlterLeadAgregado($dataOld,$data));
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new LeadAgregadoRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return response()->json($destroy,400);
        return response()->json(['message'=>'Deletado com sucesso'],200);
    }

}