<?php

namespace Modules\Client\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\Campanha;
use Modules\Client\Entities\Lead;
use Modules\Client\Entities\RedeCredenciada;
use Modules\Client\Repositories\CampanhaRepository;
use Modules\AytyProvider\Http\Controllers\AytyLogsController;


class CampanhaController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    public function getCampanhaByUrl($busca)
    {
        $repository = new CampanhaRepository;
        $builder = $repository->builder();
        $find = $builder->where('campanha_url',$busca)
            ->orWhere('campanha_nome','like','%'.$busca.'%')->first();
        if(is_null($find)) return response()->json(['campanha_id'=>null],400);
        return response()->json(['campanha_id'=>$find->campanha_id],200);
    }
    //retorna item paginados
    public function index($busca = null)
    {
        $repository = new CampanhaRepository;
        $builder = $repository->builder()->with([
            'usuario'=>function($query) {
                $query->select('usuario_id','login');
            },
            'produto'=>function($query) {
                $query->select('produto_id','produto_nome','seguradora_id')
                    ->with(['seguradora'=>function($query){
                    $query->select('seguradora_id','seguradora_nome');
                }]);
            }
        ]);
        if(!is_null($busca)){
            $builder->where('campanha_nome','like','%'.$busca.'%');
        }
        return $builder->paginate();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new CampanhaRepository;
        $save = $repository->store($data);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new CampanhaRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new CampanhaRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

    public function getLeadsFacebookByAdsID($ads_id,$camp_id)
    {
        $sem_redecredenciada = 0;
        $importados = 0;
        $duplicados = 0;
        $incosistentes = 0;

        $formatFone = function($fone){
            $newFone = str_replace("+55","",$fone);
            $newFone = str_replace(" ","",$newFone);
            $newFone = str_replace("-","",$newFone);
            return $newFone;
        };
        $url = getenv("FB_URL");
        $token = getenv("FB_TOKEN");

        $request = $this->client->request(
            'GET',
            $url.'/'.$ads_id.'/leads?access_token='.$token.'&limit=15000');
        $data = json_decode($request->getBody()->getContents());
        //return json_encode($data);
        $newData = [];
        $i=0;
        foreach ($data->data as $itens){
            $findLead = Lead::where('lead_code_import',$itens->id)->first();
            if(!is_null($findLead)) {
                continue;
            }
            $newData[$i]['lead_code_import'] = $itens->id;
            $newData[$i]['lead_nome'] = "";
            $newData[$i]['lead_email'] = "";
            $newData[$i]['lead_uf'] = "";
            $newData[$i]['lead_cidade'] = "";
            $newData[$i]['lead_fone'] = "";
            foreach ($itens->field_data as $lead){
                $newData[$i]['lead_nome'] = $lead->name=="full_name" ? $lead->values[0] : $newData[$i]['lead_nome'];
                $newData[$i]['lead_email'] = $lead->name=="email" ? $lead->values[0] : $newData[$i]['lead_email'];
                $newData[$i]['lead_uf'] = $lead->name=="state" ? $lead->values[0] : $newData[$i]['lead_uf'];
                $newData[$i]['lead_cidade'] = $lead->name=="city" ? $lead->values[0] : $newData[$i]['lead_cidade'];
                $newData[$i]['lead_fone'] = $lead->name=="phone_number" ? $formatFone($lead->values[0]) : $newData[$i]['lead_fone'];
            }
            $findRede = RedeCredenciada::where('uf','like','%'.strtoupper($newData[$i]['lead_uf']).'%')
                ->where('cidade','like','%'.strtoupper($newData[$i]['lead_cidade']).'%')
                ->first();
            //se tem rede credenciada
            if(!is_null($findRede)){
                $newData[$i]['lead_tag'] = "importar_ayty";
            } else if(strlen($newData[$i]['lead_uf']) > 2 ) {
                $newData[$i]['lead_tag'] = "inconsistente_estado";
                $incosistentes++;
            } else {
                $newData[$i]['lead_tag'] = "sem_redecredenciada";
                $sem_redecredenciada++;
            }
            $newData[$i]['camapanha_id'] = $camp_id;
            $newData[$i]['lead_has_exported'] = 'facebook';

            $findLead = Lead::where('lead_email',$newData[$i]['lead_email'])->first();

            if(is_null($findLead)) {
                $newData[$i]['lead_criacao'] = date('Y-m-d H:i:s');
                $lead_id = Lead::create($newData[$i])->lead_id;
                //importar ayty
                if($newData[$i]['lead_tag']=="importar_ayty") {
                    //CODE IMPORTACAO AYTY
                    $newData[$i]['lead_id'] = $lead_id;
                    (new AytyLogsController)->newLead($newData[$i]);
                }
                $importados++;
            } else {
                Lead::where('lead_email',$newData[$i]['lead_email'])->increment('lead_hits_interest');
                $duplicados++;
            }
            $i++;
        }
        $objResp = [
            'sem_redecredenciada'=>$sem_redecredenciada,
            'num_importados'=>$importados,
            'incosistentes'=>$incosistentes,
            'duplicados'=>$duplicados,
            'disponiveis'=>($i-($incosistentes+$duplicados)),
            'total'=>$i
        ];
        return $objResp;
    }

    public function getLeadGenForms($formName){
        set_time_limit(0);
        try {
            $camapaign = $this->getCampanhaByUrl($formName);
            if($camapaign->getStatusCode()!=200) return response()->json(['message'=>'Camapanha não existe'],400);
            $id_camp = $camapaign->getData()->campanha_id;
            $act = "act_".getenv("FB_US_ID");
            $url = getenv("FB_URL");
            $r = $this->client->request("GET",$url."/".$act."/leadgen_forms?limit=1000000&access_token=".getenv('FB_TOKEN'));
            $data = json_decode($r->getBody()->getContents());
            foreach ($data->data as $ads){
                if($formName==$ads->name){
                    return $this->getLeadsFacebookByAdsID($ads->id,$id_camp);
                }
            }
            return response()->json(['message'=>"AD não encontrada!"],400);
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }
}