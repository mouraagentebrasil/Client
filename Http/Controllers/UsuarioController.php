<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Repositories\UsuarioRepository;
use Modules\Client\Repositories\UsuarioTokenValidoRepository;
use Modules\Client\Entities\Usuario;
use Modules\Client\Entities\GrupoRota;
use Modules\Client\Entities\GrupoUsuario;
use Modules\Client\Entities\Grupo;
use Modules\Client\Entities\Rota;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    //
    public function storeWithGrupo(Request $request)
    {
        $data = $request->all();
        $grupo = $data['grupo'];
        //return $grupo;
        unset($data['grupo']);
        $data['usuario_criacao'] = date('Y-m-d H:i:s');
        if(isset($data['password']) != isset($data['password_re'])){
            return response()->json(['message'=>['Senhas não conferem']],400);
        }
        unset($data['password_re']);
        $validar = (new Usuario())->validate($data,['usuario_criacao','fl_ativo','fl_lead']);
        if(is_array($validar)) return response()->json($validar,400);
        try {
            DB::beginTransaction();
                $usuario = Usuario::create($data);
                $id_usuario = $usuario->usuario_id;
                for($i=0;$i<count($grupo);$i++){
                    GrupoUsuario::create(['usuario_id'=>$id_usuario,'grupo_id'=>$grupo[$i]]);
                }
            DB::commit();
            return response()->json(['message'=>'Salvo com sucesso']);
        } catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();
        }
    }

    //
    public function updateWithGrupo(Request $request)
    {
        $data = $request->all();
        if($data['password']=="" || !isset($data['password']))  unset($data['password']);
        $validar = (new Usuario())->validate($data,['usuario_criacao','fl_ativo','fl_lead','password']);
        if(is_array($validar)) return response()->json($validar,400);
        try {
            DB::beginTransaction();
            $new = $data;
            unset($new['usuario_id']);
            unset($new['grupo_id']);
            Usuario::find($data['usuario_id'])->update($new);
            Usuario::find($data['usuario_id'])->GrupoUsuarioB()->sync([$data['grupo_id']]);
            DB::commit();
            return response()->json(['message'=>'Salvo com sucesso']);
        } catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();
        }
    }
    //retorna item paginados
    public function index()
    {
        $model = new Usuario();
        return $model->select(['usuario_id','login','email','usuario_telefone','fl_ativo'])->with(['GrupoUsuario'=>function($query){
            $query->with('Grupo');
        }])->get();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new UsuarioRepository;
        $data['usuario_criacao'] = date('Y-m-d H:i:s');
        $save = $repository->store($data,['fl_ativo','fl_lead']);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new UsuarioRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new UsuarioRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

}