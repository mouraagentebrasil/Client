<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Repositories\ProdutoRepository;

class ProdutoController extends Controller
{
    public function view($produto_id = null)
    {
        $repository = new ProdutoRepository;
        $join = $repository->builder()->with([
            'seguradora'=>function($query){
                $query->select('seguradora_id','seguradora_nome');
            },
            'usuario'=>function($query){
                $query->select('usuario_id','login');
            }]);

        if(!is_null($produto_id)) {
            $join->where('produto_id',$produto_id);
        }
        return $join->first();
    }
    //retorna item paginados
    public function index($produto_nome = null)
    {
        $repository = new ProdutoRepository;
        $join = $repository->builder()->with([
            'seguradora'=>function($query){
                $query->select('seguradora_id','seguradora_nome');
            },
            'usuario'=>function($query){
            $query->select('usuario_id','login');
        }]);

        if(!is_null($produto_nome)) {
            $join->where('produto_nome','like','%'.$produto_nome.'%');
        }
        return $join->get();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new ProdutoRepository;
        $save = $repository->store($data,['legado_importacao']);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new ProdutoRepository;
        $update = $repository->update($id,$data,['legado_importacao']);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new ProdutoRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

}