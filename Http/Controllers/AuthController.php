<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Client\Entities\GrupoRota;
use Modules\Client\Entities\GrupoRotaFrontend;
use Modules\Client\Entities\Usuario;
use Modules\Client\Repositories\UsuarioTokenValidoRepository;
use Modules\Client\Entities\UsuarioTokenValido;
use Modules\Client\Entities\GrupoUsuario;

class AuthController extends Controller
{
    public function getTokenUsuarioByLogin(Request $request)
    {
        $data = $request->all();
        $modelUsuario = new Usuario();

        if((!isset($data['login']) || !isset($data['password'])) ||
            is_null($data['login'])||is_null($data['password'])) return response()->json([
            'message'=>'Favor informe login ou senha'
        ],400);

        $find = $modelUsuario->select(['usuario_id','login','email'])
            ->where('login',$data['login'])
            ->where('password',sha1($data['password']))
            ->first();
        if(is_null($find)) return response()->json([
            'message'=>'Usuario não encontrado'
        ],400);

        $ret = [];
        $ret['usuario'] = $find->toArray();
        $ret['grupo_rota'] = $this->informationGroup($find->usuario_id);
        $token = $ret;
        $token['ttl'] = time()+(60*60);
        $ret['token'] = base64_encode(json_encode($token));
        $this->saveOrUpdateTokenValid($find->usuario_id,$ret['token']);
        $ret['front_end'] = $this->getRoutesFrontEnd($find->usuario_id);
        return $ret;
    }

    public function getRoutesFrontEnd($usuario_id)
    {
        $ret = [];
        $modelGrupoUsuario = new GrupoUsuario();
        $getGrupoUsuario = $modelGrupoUsuario->where('usuario_id',$usuario_id)->first();
        if(is_null($getGrupoUsuario)) {
            //é um lead ou usuario sem credenciais
            return $ret;
        }
        $modelRotasFrontend = new GrupoRotaFrontend();
        $findRotasFrontend = $modelRotasFrontend->with('RotaFrontend')->where('grupo_id',$getGrupoUsuario->grupo_id)->get();
        return $findRotasFrontend;
    }

    public function saveOrUpdateTokenValid($usuario_id,$token)
    {
        $repositoryTokenValido = new UsuarioTokenValidoRepository();
        $modelRepositoryValid = new UsuarioTokenValido();
        if($modelRepositoryValid->where('usuario_id',$usuario_id)->first() == null){
            $repositoryTokenValido->store([
                'token_valido'=>$token,
                'date_request'=>date('Y-m-d H:i:s'),
                'usuario_id'=>$usuario_id,
                'is_valid'=>1
            ]);
        } else {
            $modelRepositoryValid->where('usuario_id',$usuario_id)->update([
                'token_valido'=>$token,
                'date_request'=>date('Y-m-d H:i:s'),
                'usuario_id'=>$usuario_id,
                'is_valid'=>1
            ]);
        }
    }

    public function informationGroup($usuario_id)
    {
        $ret = [];
        $modelGrupoUsuario = new GrupoUsuario();
        $getGrupoUsuario = $modelGrupoUsuario->where('usuario_id',$usuario_id)->first();

        $ret['grupo'] = $getGrupoUsuario;
        $ret['grupo']['name'] = $getGrupoUsuario->Grupo->grupo_nome;
        $ret['rotas'] = [];

        $modelGrupoRota = new GrupoRota();
        $getGrupoRotas = $modelGrupoRota->where('grupo_id',$getGrupoUsuario->grupo_id)->get();
        foreach ($getGrupoRotas as $item){
            $ret['rotas'][] = $item->Rota->rota_id;
        }
        return $ret;
    }

    public function getNewTokenByTokenValid(Request $request)
    {
        $data = $request->header('authorization');
        if(strrpos($data,"Bearer ")===false) return response()->json(['message'=>'Token fora de padrão'],400);

        $data = str_replace("Bearer ","",$data);
        $modelTokenValido = new UsuarioTokenValido();
        $checkToken = $modelTokenValido->where('token_valido',$data)->where('is_valid',1)->first();
        if(is_null($checkToken)) return response()->json(['message'=>'Token passado é invalido'],400);

        $decrypt = json_decode(base64_decode($data));
        if(time() > $decrypt->ttl) return response()->json(['message'=>'Token passado expirou'],400);

        $decrypt->ttl = time()+60*60;
        $hash = base64_encode(json_encode($decrypt));

        $this->saveOrUpdateTokenValid($decrypt->usuario->usuario_id,$hash);
        //$modelTokenValido->where('token_valido',$data)->update(['is_valid'=>0],"*");
        return ['token'=>$hash];
    }

    public function setTokenLogout(Request $request)
    {
        $data= $request->header('authorization');
        $data = str_replace("Bearer ","",$data);
        $usuariotoken = new UsuarioTokenValido();
        if($usuariotoken->where('token_valido',$data)->update(['is_valid'=>0],"*")){
            return response()->json(['message'=>"até a proxima"],200);
        }
        return response()->json(['message'=>"login invalido"],400);
    }

}
