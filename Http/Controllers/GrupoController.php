<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\Grupo;
use Modules\Client\Entities\GrupoRota;
use Modules\Client\Entities\GrupoRotaFrontend;
use Modules\Client\Entities\Rota;
use Modules\Client\Repositories\GrupoRepository;

class GrupoController extends Controller
{

    public function storeGrupoRota(Request $request)
    {
        $data = $request->all();
        //return $data;
        $modelGrupo = new Grupo();
        $modelRotas = new Rota();
        $modelGrupoRota = new GrupoRota();
        $modelGrupoRotaFrontend = new GrupoRotaFrontend();
        if(!isset($data['grupo_nome']) || $data['grupo_nome'] == ""){
            return response()->json(['message'=>'Favor preencha o nome do grupo']);
        }
        if(empty($data['rotas'])){
            return response()->json(['message'=>'Escolha pelo menos uma rota']);
        }
        try {
            DB::beginTransaction();
            $dataGrupo = $modelGrupo->create(['grupo_nome'=>$data['grupo_nome'],'grupo_criacao'=>date('Y-m-d')]);
            $grupoID = $dataGrupo->grupo_id;

            for($i=0;$i<count($data['rotas']);$i++){
                $modelGrupoRota->create(['rota_id'=>$data['rotas'][$i],'grupo_id'=>$grupoID]);
            }

            for($k=0;$k<count($data['rotas_frontend']);$k++){
                $modelGrupoRotaFrontend->create(['rota_frontend_id'=>$data['rotas_frontend'][$k],'grupo_id'=>$grupoID]);
            }

            DB::commit();
            return ['message'=>'Grupo salvo com sucesso'];
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }

    public function updateGrupoRota(Request $request)
    {
        $data = $request->all();
        //return $data;
        $modelGrupo = new Grupo();
        $modelRotas = new Rota();
        $modelGrupoRota = new GrupoRota();
        $modelGrupoRotaFrontend = new GrupoRotaFrontend();
        if(!isset($data['grupo_nome']) || $data['grupo_nome'] == ""){
            return response()->json(['message'=>'Favor preencha o nome do grupo']);
        }
        if(empty($data['rota'])){
            return response()->json(['message'=>'Escolha pelo menos uma rota']);
        }
        try {
            DB::beginTransaction();
            $dataGrupo = $modelGrupo->where('grupo_id',$data['grupo_id'])->update(['grupo_nome'=>$data['grupo_nome']]);
            $grupoID = $data['grupo_id'];
            Grupo::find($grupoID)->GrupoRotaB()->sync($data['rota']);
            Grupo::find($grupoID)->GrupoRotaFrontendB()->sync($data['rota_frontend']);
            DB::commit();
            return ['message'=>'Grupo salvo com sucesso'];
        } catch (\Exception $e){
            DB::rollback();
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }

    //retorna item paginados
    public function index()
    {
        $model = new Grupo();
        $data = $model->with(['GrupoRota'=>function($query){ $query->with('Rota'); },'GrupoRotaFrontend'=>function($query){ $query->with('RotaFrontend'); }])->get();
        return $data;
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new GrupoRepository;
        $save = $repository->store($data);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new GrupoRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new GrupoRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

}