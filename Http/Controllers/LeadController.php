<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;
use Modules\Client\Entities\Lead;
use Modules\Client\Entities\LeadAgregado;
use Modules\Client\Entities\RedeCredenciada;
use Modules\Client\Events\EventRegisterLead;
use Modules\Client\Repositories\LeadRepository;
use Modules\Client\Events\EventAlterRegisterLead;
use Modules\AytyProvider\Http\Controllers\AytyLogsController;

class LeadController extends Controller
{
    //retorna item paginados
    public function index($buscar = null)
    {
        $repository = new LeadRepository;
        $builder = $repository->builder()->with(['campanha'=>function($query) use ($buscar) {
            $query->select('campanha_id','campanha_nome','produto_id')->with(['Produto'=>function($query){
                $query->select('produto_id','produto_nome');
            }]);
        }]);
        if(!is_null($buscar)){
            $builder->where('lead_nome','like','%'.$buscar.'%')
                ->orWhere('lead_cpf',$buscar)
                ->orWhere('lead_id',$buscar)
                ->orWhere('lead_fone',$buscar)
                ->orWhere('lead_email',$buscar);
        }
        return $builder->orderBy('lead_id','desc')->paginate();
    }

    //retorna os leads agregados de um lead
    public function getAggregatesByLead($id)
    {
        $repository = new LeadRepository;
        $builder = $repository->builder()
            ->find($id)
            ->LeadAgregado();
        $page = $builder->get();
        return $page;
    }

    //metodo para salvar um novo registro
    public function store(Request $request, $dados = null)
    {
        $data = is_null($dados) ? $request->all() : $dados;
        $repository = new LeadRepository;
        $data['lead_criacao'] = date("Y-m-d H:i:s");
        $save = $repository->store($data,[
            'lead_bairro','lead_cep','lead_cpf','lead_cidade',
            'lead_capturado','lead_cobranca','lead_cpf',
            'lead_saldo','usuario_id','lead_logradouro','lead_nascimento',
            'campanha_id','lead_geracao','lead_uf',
            'lead_ip','lead_sexo','lead_cobrado','lead_numero',
            'lead_status','lead_complemento','lead_estado_civil','lead_mae',
            'lead_custo','lead_has_exported','lead_tag','lead_hits_interest',
            'lead_code_import','lead_legado_id','lead_legado_importacao','lead_object_ayty','lead_ayty_num_call',
            'lead_ayty_num_call_finalizado'
        ]);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        event(new EventRegisterLead($save));
        $data['lead_id'] = $save;
        if(!empty($data['lead_uf']) && !empty($data['lead_cidade'])){
            $isRede = RedeCredenciada::where('uf','like','%'.$data['lead_uf']."%")->where('cidade','like','%'.$data['lead_cidade'].'%')->first();
            if(!is_null($isRede)){
                (new AytyLogsController)->newLead($data);
            } else {
                Lead::where('lead_id',$data['lead_id'])->update(['lead_tag'=>'sem_redecredenciada']);
            }
        }

        if(is_null($dados)) return response()->json(['message'=>'Salvo com sucesso'],200);
        return $data['lead_id'];
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new LeadRepository;
        $dataOld = $repository->builder()->find($id);
        $dataA = $dataOld->toArray();
        //if($dataA==)
        $update = $repository->update($id,$data,'*');
        //testa se o respositorio retorna um erro,
        if(is_array($update)) return response()->json($update,400);

        if($data['lead_cobrado']){
            $this->changeAgregado($dataA,$data);
        }
        //evento de registro de alteração de lead
        $data['lead_id'] = $id;
        event(new EventAlterRegisterLead($dataOld,$data));
        return response()->json(['message'=>'Salvo com sucesso'],200);

    }

    public function changeAgregado($objL,$data){
        //monta um array com as diferenças do lead antigo para o novo
        $data_new = array_merge($objL,$data);

        //Existe um lead agregado titular
        $findAgregado = LeadAgregado::whereIn('lead_agregado_tipo',['titular','titular_pagador'])
            ->where('lead_agregado_cpf',$objL['lead_cpf'])
            ->where('lead_id',$objL['lead_id'])
            ->first();
        //monta um array compativel com lead_agregado
        $objectNew = [
            'lead_agregado_tipo'=>'titular_pagador',
            'lead_agregado_nome'=>$data_new['lead_nome'],
            'lead_agregado_cpf'=>$data_new['lead_cpf'],
            'lead_agregado_nascimento'=>$data_new['lead_nascimento'],
            'lead_agregado_mae'=>$data_new['lead_mae'],
            'lead_agregado_email'=>$data_new['lead_email'],
            'lead_agregado_fone'=>$data_new['lead_fone'],
            'lead_agregado_cep'=>$data_new['lead_cep'],
            'lead_agregado_uf'=>$data_new['lead_uf'],
            'lead_agregado_cidade'=>$data_new['lead_cidade'],
            'lead_agregado_bairro'=>$data_new['lead_bairro'],
            'lead_agregado_logradouro'=>$data_new['lead_logradouro'],
            'lead_agregado_complemento'=>$data_new['lead_complemento'],
            'lead_agregado_numero'=>$data_new['lead_numero'],
            'lead_agregado_sexo'=>$data_new['lead_sexo'],
            'fl_ativo'=>0,
            'lead_id'=>$data_new['lead_id']
        ];
        if($data_new['lead_cobrado'] == "pagador") $objectNew['fl_ativo'] = 0;
        if($data_new['lead_cobrado'] == "titular_pagador") $objectNew['fl_ativo'] = 1;
        //if($data_new['lead_cobrado'] == "titular") $objectNew['fl_ativo'] = 1;
        //se não existe um lead_agregado salva um novo, se houver apenas atualiza
        $checkTitular = (new LeadAgregadoController())->checkLeadTitular($data_new['lead_id']);
        $objectNew['fl_ativo'] = $checkTitular ? 0 : 1;
        if(is_null($findAgregado)){
            LeadAgregado::create($objectNew);
        } else {
            LeadAgregado::find($findAgregado->lead_agregado_id)->update($objectNew);
        }
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new LeadRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

    //obtem leads por dia
    public function getLeadByRange($dateI=null,$dateF=null)
    {
        $dateI = is_null($dateI) ? date('Y-m-d') : $dateI;
        $dateF = is_null($dateF) ? $dateI : $dateF;
        $leads = Lead::where('lead_criacao','>=',$dateI." 00:00:00")
            ->where('lead_criacao','<=',$dateF." 23:59:59")
            ->get();
        return response()->json($leads,200);
    }

}