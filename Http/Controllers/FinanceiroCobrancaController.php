<?php

namespace Modules\Client\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\FinanceiroCobranca;
use Modules\Client\Entities\FinanceiroContratacao;
use Modules\Client\Repositories\FinanceiroCobrancaRepository;

class FinanceiroCobrancaController extends Controller
{
    
    //retorna item paginados
    public function index()
    {
        $repository = new FinanceiroCobrancaRepository;
        return $repository->builder()->paginate();
    }
    //metodo para salvar um novo registro
    public function store(Request $request)
    {
        $data = $request->all();
        $repository = new FinanceiroCobrancaRepository;
        $save = $repository->store($data);
        //testa se o respositorio retorna um erro
        if(is_array($save)) return response()->json($save,400);
        return response()->json(['message'=>'Salvo com sucesso'],200);
    }
    //atualiza um registro
    public function update(Request $request,$id)
    {
        $data = $request->all();
        $repository = new FinanceiroCobrancaRepository;
        $update = $repository->update($id,$data);
        //testa se o respositorio retorna um erro
        if(is_array($update)) return response()->json($update,400);
        return response()->json(['message'=>'Atualizado com sucesso'],200);
    }
    //destruir um item
    public function destroy($id)
    {
        $repository = new FinanceiroCobrancaRepository;
        $destroy = $repository->delete($id);
        if(is_array($destroy)) return $destroy;
        return response()->json(['message'=>'Deletado com sucesso'],400);
    }

    public function getCobrancas($filtro, $dateI = null,$dateF = null)
    {

        if(is_null($dateI)) $dateI = date("Y-m-d");
        if(is_null($dateF)) $dateF = $dateI;

        $finCob = FinanceiroCobranca::with([
            'Usuario'=>function($query){
                $query->select(['usuario_id','login']);
            },
            'FinanceiroContratacao'=>function($query){
                $query->with(['LeadProdutoAgregado'=>function($query){
                    $query->with(['Lead',
                        'Produto'=>function($query){
                            $query->with('Seguradora');
                        }]);
                }]);
            }
        ])->where('data_vencimento','>=',$dateI)->where('data_vencimento','<=',$dateF);
        //filtros
        if(in_array($filtro,['pagos','npagos'])){
            $finCob->where('foi_pago',$filtro=="pagos" ? 1 : 0);
        } else if(in_array($filtro,['faturado','nfaturado'])){
            if($filtro=="faturado"){
                $finCob->where('gateway_current_code','<>',NULL);
            } else {
                $finCob->where('gateway_current_code',NULL);
            }
        }
        //
        $finCob = $finCob->get()->toArray();
        $newData = [];
        if(!empty($finCob)){
            $total_arrecadado = 0;
            $total_geral = 0;
            $total_pagos = 0;
            $qtd_boleto = 0;
            $valor_boleto = 0;
            $qtd_card = 0;
            $valor_card = 0;

            $newData['info_cob'] = $finCob;
            $newData['stats_cob'] = [];
            foreach ($newData['info_cob'] as $cobs) {
                $total_geral += $cobs['valor'];
                $total_arrecadado += $cobs['valor_pago'];
                if($cobs['foi_pago']) $total_pagos++;
                //boleto
                if($cobs['financeiro_forma_pagamento_id']==1 && $cobs['foi_pago']==1) {
                    $qtd_boleto++;
                    $valor_boleto += $cobs['valor_pago'];
                }
                //cartao
                if($cobs['financeiro_forma_pagamento_id']==2 && $cobs['foi_pago']==1) {
                    $qtd_card++;
                    $valor_card += $cobs['valor_pago'];
                }
            }
            $newData['stats_cob']['total_geral'] = $total_geral;
            $newData['stats_cob']['total_arrecadado'] = $total_arrecadado;
            $newData['stats_cob']['total_pagos'] = $total_pagos;
            $newData['stats_cob']['valor_cartao'] = $valor_card;
            $newData['stats_cob']['qtd_cartao'] = $qtd_card;
            $newData['stats_cob']['valor_boleto'] = $valor_boleto;
            $newData['stats_cob']['qtd_boleto'] = $qtd_boleto;


        }
        return $newData;
    }

    public function updateCobranca($cobranca_id,$vendedor_id)
    {
        try {
            DB::beginTransaction();
            $find = FinanceiroCobranca::findOrFail($cobranca_id);
            $find->update([
                'usuario_id'=>$vendedor_id
            ]);
            DB::commit();
            return response()->json(['message'=>"Cobrança atualizada!"],200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }
}