<?php

Route::group(['middleware' => ['web','cors'], 'prefix' => 'client', 'namespace' => 'Modules\Client\Http\Controllers'], function()
{
    Route::post('/auth/getTokenByLogin','AuthController@getTokenUsuarioByLogin');
    Route::get('/auth/getNewTokenByTokenValid','AuthController@getNewTokenByTokenValid');
    Route::post('/lead/store', 'LeadController@store');
    //api externa
    Route::get('/campanha/getCampanhaByUrl/{campanha_url}', 'CampanhaController@getCampanhaByUrl');
    //rotas protegidas por login-token e acl
    Route::group(['middleware' => 'auth_client'],function(){
        //Route::post('/auth/getTokenByLogin','AuthController@getTokenUsuarioByLogin');
        Route::get('/auth/setTokenLogout','AuthController@setTokenLogout');
        //Rotas para lead
        Route::get('/lead/{buscar?}', 'LeadController@index');
        //Route::post('/lead/store', 'LeadController@store');
        Route::post('/lead/update/{id}','LeadController@update');
        Route::get('/lead/destroy/{id}','LeadController@destroy');
        Route::get('/lead/getAggregatesByLead/{id}','LeadController@getAggregatesByLead');
        //Rotas para usuario
        Route::get('/usuario', 'UsuarioController@index');
        Route::post('/usuario/storeWithGrupo', 'UsuarioController@storeWithGrupo');
        Route::post('/usuario/store', 'UsuarioController@store');
        Route::post('/usuario/updateWithGrupo', 'UsuarioController@updateWithGrupo');
        //Route::post('/usuario/update/{id}','UsuarioController@update');
        Route::get('/usuario/destroy/{id}','UsuarioController@destroy');
        //Rotas para log_usuario_login
        Route::get('/log_usuario_login', 'LogUsuarioLoginController@index');
        Route::post('/log_usuario_login/store', 'LogUsuarioLoginController@store');
        Route::post('/log_usuario_login/update/{id}','LogUsuarioLoginController@update');
        Route::get('/log_usuario_login/destroy/{id}','LogUsuarioLoginController@destroy');
        //Rotas para usuario_reset_password
        Route::get('/usuario_reset_password', 'UsuarioResetPasswordController@index');
        Route::post('/usuario_reset_password/store', 'UsuarioResetPasswordController@store');
        Route::post('/usuario_reset_password/update/{id}','UsuarioResetPasswordController@update');
        Route::get('/usuario_reset_password/destroy/{id}','UsuarioResetPasswordController@destroy');
        //Rotas para usuario_token_valido
        Route::get('/usuario_token_valido', 'UsuarioTokenValidoController@index');
        Route::post('/usuario_token_valido/store', 'UsuarioTokenValidoController@store');
        Route::post('/usuario_token_valido/update/{id}','UsuarioTokenValidoController@update');
        Route::get('/usuario_token_valido/destroy/{id}','UsuarioTokenValidoController@destroy');
        //Rotas para grupo
        Route::get('/grupo', 'GrupoController@index');
        Route::post('/grupo/storeGrupoRota', 'GrupoController@storeGrupoRota');
        Route::post('/grupo/updateGrupoRota','GrupoController@updateGrupoRota');
        Route::post('/grupo/store', 'GrupoController@store');
        Route::post('/grupo/update/{id}','GrupoController@update');
        Route::get('/grupo/destroy/{id}','GrupoController@destroy');
        //Rotas para grupo_usuario
        Route::get('/grupo_usuario', 'GrupoUsuarioController@index');
        Route::post('/grupo_usuario/store', 'GrupoUsuarioController@store');
        Route::post('/grupo_usuario/update/{id}','GrupoUsuarioController@update');
        Route::get('/grupo_usuario/destroy/{id}','GrupoUsuarioController@destroy');
        //Rotas para grupo_rota
        Route::get('/grupo_rota', 'GrupoRotaController@index');
        Route::post('/grupo_rota/store', 'GrupoRotaController@store');
        Route::post('/grupo_rota/update/{id}','GrupoRotaController@update');
        Route::get('/grupo_rota/destroy/{id}','GrupoRotaController@destroy');
        //Rotas para rota
        Route::get('/rota', 'RotaController@index');
        Route::post('/rota/store', 'RotaController@store');
        Route::post('/rota/update/{id}','RotaController@update');
        Route::get('/rota/destroy/{id}','RotaController@destroy');
        //Rotas para seguradora
        Route::get('/seguradora', 'SeguradoraController@index');
        Route::post('/seguradora/store', 'SeguradoraController@store');
        Route::post('/seguradora/update/{id}','SeguradoraController@update');
        Route::get('/seguradora/destroy/{id}','SeguradoraController@destroy');
        //Rotas para produto
        Route::get('/produto/{produto?}', 'ProdutoController@index');
        Route::get('/produto/view/{produto_id?}', 'ProdutoController@view');
        Route::post('/produto/store', 'ProdutoController@store');
        Route::post('/produto/update/{id}','ProdutoController@update');
        Route::get('/produto/destroy/{id}','ProdutoController@destroy');
        //Rotas para campanha
        Route::get('/campanha/{busca?}', 'CampanhaController@index');
        Route::post('/campanha/store', 'CampanhaController@store');
        Route::post('/campanha/update/{id}','CampanhaController@update');
        Route::get('/campanha/destroy/{id}','CampanhaController@destroy');
        //Rotas para lead_agregado
        Route::get('/lead_agregado/{buscar?}', 'LeadAgregadoController@index');
        Route::post('/lead_agregado/store', 'LeadAgregadoController@store');
        Route::post('/lead_agregado/update/{id}','LeadAgregadoController@update');
        Route::get('/lead_agregado/destroy/{id}','LeadAgregadoController@destroy');
        //Rotas para lead_life_time
        Route::get('/lead_life_time/{id}', 'LeadLifeTimeController@index');
        Route::post('/lead_life_time/store', 'LeadLifeTimeController@store');
        Route::post('/lead_life_time/update/{id}','LeadLifeTimeController@update');
        Route::get('/lead_life_time/destroy/{id}','LeadLifeTimeController@destroy');
        //Rotas para lead_life_time_operacao
        Route::get('/lead_life_time_operacao', 'LeadLifeTimeOperacaoController@index');
        Route::post('/lead_life_time_operacao/store', 'LeadLifeTimeOperacaoController@store');
        Route::post('/lead_life_time_operacao/update/{id}','LeadLifeTimeOperacaoController@update');
        Route::get('/lead_life_time_operacao/destroy/{id}','LeadLifeTimeOperacaoController@destroy');
        //Rotas para carteira
        Route::get('/carteira', 'CarteiraController@index');
        Route::post('/carteira/store', 'CarteiraController@store');
        Route::post('/carteira/update/{id}','CarteiraController@update');
        Route::get('/carteira/destroy/{id}','CarteiraController@destroy');
        //Rotas para grupo_rota_frontend
        Route::get('/grupo_rota_frontend', 'GrupoRotaFrontendController@index');
        Route::post('/grupo_rota_frontend/store', 'GrupoRotaFrontendController@store');
        Route::post('/grupo_rota_frontend/update/{id}','GrupoRotaFrontendController@update');
        Route::get('/grupo_rota_frontend/destroy/{id}','GrupoRotaFrontendController@destroy');
        //Rotas para rota_frontend
        Route::get('/rota_frontend', 'RotaFrontendController@index');
        Route::post('/rota_frontend/store', 'RotaFrontendController@store');
        Route::post('/rota_frontend/update/{id}','RotaFrontendController@update');
        Route::get('/rota_frontend/destroy/{id}','RotaFrontendController@destroy');
        //Rotas para lead_produto_agregado
        Route::get('/lead_produto_agregado', 'LeadProdutoAgregadoController@index');
        Route::post('/lead_produto_agregado/store', 'LeadProdutoAgregadoController@store');
        Route::post('/lead_produto_agregado/update/{id}','LeadProdutoAgregadoController@update');
        Route::get('/lead_produto_agregado/destroy/{id}','LeadProdutoAgregadoController@destroy');
        //other routes
        Route::get('/lead_produto_agregado/getLeadAgregadoContratacao/{lead_agregado_id}','LeadProdutoAgregadoController@getLeadAgregadoContratacao');
        //Rotas para financeiro_contratacao
        Route::get('/financeiro_contratacao', 'FinanceiroContratacaoController@index');
        Route::post('/financeiro_contratacao/store', 'FinanceiroContratacaoController@store');
        Route::post('/financeiro_contratacao/update/{id}','FinanceiroContratacaoController@update');
        Route::get('/financeiro_contratacao/destroy/{id}','FinanceiroContratacaoController@destroy');
        //Rotas para financeiro_cobranca
        Route::get('/financeiro_cobranca', 'FinanceiroCobrancaController@index');
        Route::post('/financeiro_cobranca/store', 'FinanceiroCobrancaController@store');
        Route::post('/financeiro_cobranca/update/{id}','FinanceiroCobrancaController@update');
        Route::get('/financeiro_cobranca/destroy/{id}','FinanceiroCobrancaController@destroy');
        Route::get('/financeiro_cobranca/update-cobranca-vendedor/{cobranca_id}/{usuario_id}','FinanceiroCobrancaController@updateCobranca');
        //Rotas para financeiro_forma_pagamento
        Route::get('/financeiro_forma_pagamento', 'FinanceiroFormaPagamentoController@index');
        Route::post('/financeiro_forma_pagamento/store', 'FinanceiroFormaPagamentoController@store');
        Route::post('/financeiro_forma_pagamento/update/{id}','FinanceiroFormaPagamentoController@update');
        Route::get('/financeiro_forma_pagamento/destroy/{id}','FinanceiroFormaPagamentoController@destroy');
        //
        Route::post('/termo_cobranca/store', 'TermoCobrancaController@store');
        Route::post('/termo_cobranca/update/{id}','TermoCobrancaController@update');
        Route::get('/termo_cobranca/destroy/{id}','TermoCobrancaController@destroy');
        //
        Route::get('/financeiro-ab/getCobrancas/{filtro}/{dateI?}/{dateF?}','FinanceiroCobrancaController@getCobrancas');
        Route::get('/financeiro-ab/getContratacao/{lead_id}','FinanceiroContratacaoController@getContratacao');
        //Rotas para operacoes_carteira
        Route::get('/operacoes_carteira', 'OperacoesCarteiraController@index');
        Route::post('/operacoes_carteira/store', 'OperacoesCarteiraController@store');
        Route::post('/operacoes_carteira/update/{id}','OperacoesCarteiraController@update');
        Route::get('/operacoes_carteira/destroy/{id}','OperacoesCarteiraController@destroy');
        //Rotas para produto_gateway
        Route::get('/produto_gateway', 'ProdutoGatewayController@index');
        Route::post('/produto_gateway/store', 'ProdutoGatewayController@store');
        Route::post('/produto_gateway/update/{id}','ProdutoGatewayController@update');
        Route::get('/produto_gateway/destroy/{id}','ProdutoGatewayController@destroy');
        //Rede Credenciada
        Route::post('/rede_credenciada/store', 'RedeCredenciadaController@store');
        Route::post('/rede_credenciada/update/{id}','RedeCredenciadaController@update');
        Route::get('/rede_credenciada/destroy/{id}','RedeCredenciadaController@destroy');
        //Rotas para log_redecredenciada
        Route::get('/log_redecredenciada', 'LogRedecredenciadaController@index');
        Route::post('/log_redecredenciada/store', 'LogRedecredenciadaController@store');
        Route::post('/log_redecredenciada/update/{id}','LogRedecredenciadaController@update');
        Route::get('/log_redecredenciada/destroy/{id}','LogRedecredenciadaController@destroy');
        //Rotas para lead_card
        Route::get('/lead_card', 'LeadCardController@index');
        Route::post('/lead_card/store', 'LeadCardController@store');
        Route::post('/lead_card/update/{id}','LeadCardController@update');
        Route::get('/lead_card/destroy/{id}','LeadCardController@destroy');
        //Rotas para log_financeiro_cobranca
        Route::get('/log_financeiro_cobranca', 'LogFinanceiroCobrancaController@index');
        Route::post('/log_financeiro_cobranca/store', 'LogFinanceiroCobrancaController@store');
        Route::post('/log_financeiro_cobranca/update/{id}','LogFinanceiroCobrancaController@update');
        Route::get('/log_financeiro_cobranca/destroy/{id}','LogFinanceiroCobrancaController@destroy');
    });
    //
    // Rotas desprotegidas de login (publicas)
    //
    Route::get('/getLeadGenForms/{form_name}','CampanhaController@getLeadGenForms');
    Route::get('/get-leads-facebook-by-campname','CampanhaController@getLeadsFacebookByCampName');
    //
    Route::post('/lead_produto_agregado/venda_produto', 'LeadProdutoAgregadoController@vendaProdutoPipeline');
    //Rotas para lead_produto_agregado_itens
    Route::get('/lead_produto_agregado_itens', 'LeadProdutoAgregadoItensController@index');
    Route::post('/lead_produto_agregado_itens/store', 'LeadProdutoAgregadoItensController@store');
    Route::post('/lead_produto_agregado_itens/update/{id}','LeadProdutoAgregadoItensController@update');
    Route::get('/lead_produto_agregado_itens/destroy/{id}','LeadProdutoAgregadoItensController@destroy');

    Route::get('/termos-de-aceite/{contracao_id}','TermosAceiteClienteController@index');
    Route::post('/termos-de-aceite/update-lead','TermosAceiteClienteController@updateLeadTermoCobranca');
    Route::post('/termos-de-aceite/update-lead-agregado','TermosAceiteClienteController@updateLeadAgregadoTermoCobranca');
    Route::get('/termos-de-aceite/confirmacao/{contratacao_id}','TermosAceiteClienteController@confirmaTermoCobranca');
    Route::get('/termos-de-aceite/atualiza-forma-pagamento/{contratacao_id}','TermosAceiteClienteController@updateFormaPagamentoBoleto');
    //Rotas para termo_cobranca
    Route::get('/termo_cobranca', 'TermoCobrancaController@index');
    //Rotas para rede_credenciada
    Route::get('/rede_credenciada/getLeadRedeCredenciadaByIp/{ip}/{campanha_id}/{lead_id}','RedeCredenciadaController@getEstadoCidadeByIp');
    Route::post('/rede_credenciada/importacao', 'RedeCredenciadaController@importRedeCredenciada');
    Route::get('/rede_credenciada/getLeadRedeCredenciada/{estado}/{cidade}/{campanha_id}/{lead_id}','RedeCredenciadaController@getLeadRedeCredenciada');
    Route::get('/rede_credenciada/{seguradora_id?}/{estado?}/{cidade?}/{ip?}', 'RedeCredenciadaController@index');
    //
    //
    Route::get('/test-rem','StrategiesMetLife@generateCnab');
    Route::get('/test-rem2','ParseLayout@readRem');
    #processaRemessa
    Route::get('/processa-remessa','StrategiesMetLife@processaRemessa');
    //Route::get('/financeiro_cobranca/update-cobranca-vendedor/{cobranca_id}/{usuario_id}','FinanceiroCobrancaController@updateCobranca');
    Route::get('/get-leads-by-range/{dateI?}/{dateF?}','LeadController@getLeadByRange');
    //[CODE]

});
