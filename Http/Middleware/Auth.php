<?php

namespace Modules\Client\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Modules\Client\Entities\UsuarioTokenValido;
use Modules\Client\Repositories\UsuarioTokenValidoRepository;
use Illuminate\Support\Facades\Route;
use Modules\Client\Entities\Rota;

class Auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $data = $request->header('authorization');
        if(empty($data)) {
            return response()->json(['message'=>'Token não esta sendo passado'],400);
        }
        $checkToken = $this->checkToken($data);
        if(is_array($checkToken)){
            return response()->json($checkToken,400);
        }
        $checkAcl = $this->checkAcl($data);
        if(is_array($checkAcl)){
            return response()->json($checkAcl,400);
        }
        //$this->invalidateToken($data);
        return $next($request);
    }

    public function invalidateToken($authorization)
    {
        $data = str_replace("Bearer ","",$authorization);
        $usuariotoken = new UsuarioTokenValido();
        $usuariotoken->where('token_valido',$data)->update(['is_valid'=>0],"*");
    }

    public function checkToken($authorization)
    {
        if(strrpos($authorization,"Bearer ")===false) return response()->json(['message'=>'Token fora de padrão'],400);
        $data = str_replace("Bearer ","",$authorization);
        $modelTokenValido = new UsuarioTokenValido();
        $checkToken = $modelTokenValido->where('token_valido',$data)->where('is_valid',1)->first();
        if(is_null($checkToken)) return ['message'=>'Token passado é invalido'];
        $decrypt = json_decode(base64_decode($data));
        if(time() > $decrypt->ttl) {
            $modelTokenValido->where('token_valido',$data)->update(['is_valid'=>0]);
            return ['message'=>'Token passado expirou'];
        }
        return true;
    }

    public function checkAcl($hash)
    {
        $data = str_replace("Bearer ","",$hash);
        $decrypt = json_decode(base64_decode($data));
        $currentObjRequest = Route::getFacadeRoot()->current()->getAction();
        $currentController = $currentObjRequest['controller'];
        $getRouteId = Rota::select('rota_id')->where('route',$currentController)->first();
        if(!isset($getRouteId->rota_id)){
            return ['message'=>'Este usuario não tem permissão para esta rota'];
        } elseif(!in_array($getRouteId->rota_id,$decrypt->grupo_rota->rotas)){
           return ['message'=>'Este usuario não tem permissão para esta rota'];
        }
        return true;
    }
}
