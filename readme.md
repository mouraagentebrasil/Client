***Servico ClientAB***

Primeiramente baixe o coreab
```
$ git clone https://gitlab.com/mouraagentebrasil/coreab
$ cd coreab && cp env.example .env
$ php artisan key:generate
```

Depois entre na pasta Modules

```
$ cd Modules
$ git clone https://gitlab.com/mouraagentebrasil/Client.git
```
E voilá...

