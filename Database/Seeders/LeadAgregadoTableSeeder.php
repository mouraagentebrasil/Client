<?php

namespace Modules\Client\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\LeadAgregado;

class LeadAgregadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Faker\Provider\pt_BR\PhoneNumber($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Address($faker));

        $formatCpf = function($cpf){
            $newCpf = str_replace('.','',$cpf);
            $newCpf = str_replace('-','',$newCpf);
            return $newCpf;
        };
        //$agregado_tipo = ['titular','titular_pagador','beneficiario'];
        for($lead_id = 1;$lead_id < 501; $lead_id++):
            for($i=0;$i<2;$i++):
                $i_tipo = rand(0,2);
                LeadAgregado::create([
                    'lead_agregado_tipo'=>'beneficiario',
                    'lead_agregado_email'=>$faker->email,
                    'lead_agregado_nome'=>$faker->name,
                    'lead_agregado_fone'=>$faker->phoneNumberCleared,
                    'lead_agregado_cpf'=>$formatCpf($faker->cpf),
                    'lead_agregado_cep'=>$faker->postcode,
                    'lead_agregado_uf'=>$faker->stateAbbr,
                    'lead_agregado_cidade'=>$faker->city,
                    'lead_agregado_numero'=>rand(1,1999),
                    'lead_agregado_logradouro'=>$faker->streetPrefix()." ".$faker->name,
                    'lead_agregado_criacao'=>date('Y-m-d H:i:s'),
                    'lead_agregado_mae'=>$faker->name('female'),
                    'lead_id'=>$lead_id
                ]);
            endfor;
        endfor;
    }
}
