<?php

namespace Modules\Client\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Client\Entities\Lead;

class LeadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Faker\Provider\pt_BR\PhoneNumber($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Address($faker));

        $formatCpf = function($cpf){
            $newCpf = str_replace('.','',$cpf);
            $newCpf = str_replace('-','',$newCpf);
            return $newCpf;
        };

        for($i=0;$i<500;$i++):
            Lead::create([
               'lead_email'=>$faker->email,
               'lead_nome'=>$faker->name,
               'lead_fone'=>$faker->phoneNumberCleared,
               'lead_cpf'=>$formatCpf($faker->cpf),
               'lead_ip'=>rand(0,255).".".rand(0,255).".".rand(0,255).".".rand(0,255),
               'lead_cep'=>$faker->postcode,
               'lead_uf'=>$faker->stateAbbr,
               'lead_cidade'=>$faker->city,
               'lead_numero'=>rand(1,1999),
               'lead_logradouro'=>$faker->streetPrefix()." ".$faker->name,
               'campanha_id'=>rand(1,2),
               'lead_criacao'=>date('Y-m-d H:i:s'),
               'lead_mae'=>$faker->name('female')
            ]);
        endfor;
    }
}
