<?php

namespace Modules\Client\Events;

use Illuminate\Queue\SerializesModels;

class EventAlterRegisterLead
{
    use SerializesModels;

    public $dataOld;
    public $dataNew;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($dataOld,$dataNew)
    {
        //
        $this->dataOld = $dataOld;
        $this->dataNew = $dataNew;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
