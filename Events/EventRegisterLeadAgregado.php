<?php

namespace Modules\Client\Events;

use Illuminate\Queue\SerializesModels;

class EventRegisterLeadAgregado
{
    use SerializesModels;
    public $lead_agregado;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($leadAgregado)
    {
        //
        $this->lead_agregado = $leadAgregado;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
