<?php

namespace Modules\Client\Events;

use Illuminate\Queue\SerializesModels;

class EventRegisterLead
{
    use SerializesModels;
    public $lead_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($lead_id)
    {
        //
        $this->lead_id = $lead_id;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
