<?php

namespace Modules\Client\Events\Handlers;

use Modules\Client\Entities\LeadLifeTime;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListenerRegisterLeadAgregado implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Client\Events\EventRegisterLeadAgregado $event
     * @return void
     */
    public function handle(\Modules\Client\Events\EventRegisterLeadAgregado $event)
    {
        //
        $operacao = 3;

        $data = date('Y-m-d H:i:s');
        LeadLifeTime::create([
            'lead_id'=>$event->lead_agregado['lead_id'],
            'data_operacao'=>$data,
            'lead_life_time_retorno'=>'LeadAgregado Registrado em '.$data,
            'lead_life_time_operacao_id'=>$operacao
        ]);
    }
}
