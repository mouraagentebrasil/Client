<?php

namespace Modules\Client\Events\Handlers;

use Modules\Client\Events\EventAlterRegisterLead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Client\Entities\LeadLifeTime;

class ListenerAlterRegisterLead
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Client\Events\EventAlterRegisterLead $event
     * @return void
     */
    public function handle(\Modules\Client\Events\EventAlterRegisterLead $event)
    {
        //lead_life_time_operacao
        $operacao = 1;

        $dataOld = $event->dataOld->toArray();
        $dataNew = $event->dataNew;
        $return = '';
        foreach($dataNew as $k=>$v){
            if($dataNew[$k] != $dataOld[$k]){
                $return .= $k." alterado de '".$dataOld[$k]."' para '".$dataNew[$k]."'<br>";
            }
        }
        if($return!=''){
            LeadLifeTime::create([
                'lead_id'=>$dataNew['lead_id'],
                'data_operacao'=>date('Y-m-d H:i:s'),
                'lead_life_time_retorno'=>$return,
                'usuario_id'=>$dataNew['usuario_id'],
                'lead_life_time_operacao_id'=>$operacao
            ]);
        }
    }
}
