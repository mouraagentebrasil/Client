<?php

namespace Modules\Client\Events\Handlers;

use Modules\Client\Entities\LeadLifeTime;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ListenerAlterLeadAgregado implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Client\Events\EventAlterLeadAgregado $event
     * @return void
     */
    public function handle(\Modules\Client\Events\EventAlterLeadAgregado $event)
    {
        //lead_life_time_operacao
        $operacao = 4;

        $dataOld = $event->dataOld;
        $dataNew = $event->dataNew;
        $return = '';
        foreach($dataNew as $k=>$v){
            if($k == "usuario_id") {continue;}
            if($dataNew[$k] != $dataOld[$k]){
                $return .= $k." alterado de '".$dataOld[$k]."' para '".$dataNew[$k]."'<br>";
            }
        }
        if($return!=''){
            LeadLifeTime::create([
                'lead_id'=>$dataNew['lead_id'],
                'data_operacao'=>date('Y-m-d H:i:s'),
                'lead_life_time_retorno'=>$return,
                'usuario_id'=>$dataNew['usuario_id'],
                'lead_life_time_operacao_id'=>$operacao
            ]);
        }
    }
}
