<?php

namespace Modules\Client\Events\Handlers;

use Modules\Client\Events\EventRegisterLead;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\Client\Entities\LeadLifeTime;

class ListenerRegisterLead implements ShouldQueue
{
    use InteractsWithQueue;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \Modules\Client\Events\EventRegisterLead $event
     * @return void
     */
    public function handle(\Modules\Client\Events\EventRegisterLead $event)
    {
        //
        $operacao = 2;

        $data = date('Y-m-d H:i:s');
        LeadLifeTime::create([
            'lead_id'=>$event->lead_id,
            'data_operacao'=>$data,
            'lead_life_time_retorno'=>'Lead (lead_id->'.$event->lead_id.') entrou no sistema '.$data,
            'lead_life_time_operacao_id'=>$operacao
        ]);
    }
}
