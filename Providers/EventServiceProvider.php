<?php

namespace Modules\Client\Providers;

//use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        'Modules\Client\Events\EventAlterRegisterLead' => [
            'Modules\Client\Events\Handlers\ListenerAlterRegisterLead',
        ],
        'Modules\Client\Events\EventRegisterLead' => [
            'Modules\Client\Events\Handlers\ListenerRegisterLead',
        ],
        'Modules\Client\Events\EventRegisterLeadAgregado' => [
            'Modules\Client\Events\Handlers\ListenerRegisterLeadAgregado',
        ],
        'Modules\Client\Events\EventAlterLeadAgregado' => [
            'Modules\Client\Events\Handlers\ListenerAlterLeadAgregado',
        ]
    ];
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
