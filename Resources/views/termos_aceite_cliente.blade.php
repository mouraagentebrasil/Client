<html> 
    <title>Termos de Aceite - 
        <?=$finCob['lead_produto_agregado']['lead']['lead_nome'];?>
    </title>     
    <header>
        <script
                src="https://code.jquery.com/jquery-3.1.1.min.js"
                integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
                crossorigin="anonymous"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js"></script>
        <script src="https://ajax.microsoft.com/ajax/jquery.validate/1.7/additional-methods.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.8/jquery.mask.js"></script>
        <script src="https://agentebrasil.com/sys-v2/js/blockUI.js"></script>
        <style>
            label {
                font-weight: normal !important;
            }
            input[type="text"] {
                font-size: 10px;
                width: 250px;
            }
            select {
                font-size: 10px;
                position: relative; left:145px; top:-20px;
            }
            .logo_left {
                position: relative;
                top: 5px;
                width: 200px;
                height: 70px;
                background-image: url("<?=getenv("SERVICO_CLIENT"); ?>/metlife_logo.png");
                background-repeat: no-repeat;
                background-size: contain;
                float: left;
            }
            .logo_right {
                position: relative;
                top: 20px;
                width: 200px;
                height: 70px;
                background-image: url("<?=getenv("SERVICO_CLIENT"); ?>/logo-agente-brasil-gde.jpg");
                background-repeat: no-repeat;
                background-size: contain;
                float: right;
            }
            .txt_la {
                padding-top: 20px;

            }
            .txt_info {
                overflow-y: scroll;
                height: 100px;
                background: #dfdfdf;
                padding: 20px;
            }
            footer {
                margin-top: 50px;
                background: #013753;
                text-align: center;
                clear: both;
                padding-bottom: 20px;
                color: #ffffff;
            }
            .info_resp_financeiro{
                margin-bottom: 20px;
            }
            .data_acesso {
                margin-top: 15px;
                float: right;
            }
            .display_none {
                display: none;
            }
            .error {
                color: #cd1105;
                font-size: small;
            }
            .data_nasc {
                width: 150px !important;
            }
            .fone {
                width: 150px !important;
            }
        </style>
    </header>
    <body>
        <div class="container">
            <div class="logo_left"></div>
            <div class="logo_right"></div>
            <div style="clear: both;"></div>
            <div class="row tos">
                <span class="data_acesso">Data de Acesso: ({{date("d/m/Y")}})</span>
                <div class="col s12" style="margin-top: 30px; font-weight: bolder; text-align: center;"><h3>Contração do Plano {{$finCob['lead_produto_agregado']['produto']['produto_nome']}}</h3></div>
            </div>
            <form id="formLeadUpdate">
                <div class="row info_resp_financeiro">
                    <div class="col-md-4">
                        <h4>Informações do Pagador @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) <small><a href="#" id="edita_info">(Editar Informações)</a></small> @endif</h4><br>
                        <b>Nome:</b><input type="text" id="lead_nome" name="lead_nome" value="{{$finCob['lead_produto_agregado']['lead']['lead_nome']}}" disabled required><br>
                        <b>Email:</b><input type="text" id="lead_email" name="lead_email" value="{{$finCob['lead_produto_agregado']['lead']['lead_email']}}" disabled required><br>
                        <b>CPF:</b>{{$finCob['lead_produto_agregado']['lead']['lead_cpf']}}<br>
                        <b>Mae:</b><input type="text" id="lead_mae" name="lead_mae" value="{{$finCob['lead_produto_agregado']['lead']['lead_mae']}}" disabled required><br>
                        <b>Data Nascimento:</b><input type="text" id="lead_nascimento" name="lead_nascimento" class="data_nasc" value="{{date('d/m/Y',strtotime($finCob['lead_produto_agregado']['lead']['lead_nascimento']))}}" disabled required><br>
                        <b>Telefone:</b><input type="text" id="lead_fone" class="fone" name="lead_fone" value="{{$finCob['lead_produto_agregado']['lead']['lead_fone']}}" disabled required><br>
                        <input type="hidden" name="financeiro_contratacao_id" value="{{$finCob['financeiro_contratacao_id']}}" id="financeiro_contratacao_id">
                        <input type="hidden" name="lead_id" value="{{ $finCob['lead_produto_agregado']['lead']['lead_id'] }}" id="lead_id">
                    </div>

                    <div class="col-md-4" style="margin-top: 56px;">
                        <b>Estado:</b><input type="text" id="lead_uf" name="lead_uf" value="{{$finCob['lead_produto_agregado']['lead']['lead_uf']}}" disabled required><br>
                        <b>Cidade:</b><input type="text" id="lead_cidade" name="lead_cidade" value="{{$finCob['lead_produto_agregado']['lead']['lead_cidade']}}" disabled required><br>
                        <b>Bairro:</b><input type="text" id="lead_bairro" name="lead_bairro" value="{{$finCob['lead_produto_agregado']['lead']['lead_bairro']}}" disabled required><br>
                        <b>Logradouro:</b><input type="text" id="lead_logradouro" name="lead_logradouro" value="{{$finCob['lead_produto_agregado']['lead']['lead_logradouro']}}" disabled required><br>
                        <b>CEP:</b><input type="text" id="lead_cep" name="lead_cep" value="{{$finCob['lead_produto_agregado']['lead']['lead_cep']}}" disabled required><br>
                    </div>

                    <div class="col-md-4" style="margin-top: 56px;">
                        <b>Data Recorrencia:</b> {{$finCob['dia_recorrencia']}}<br>
                        <b>Valor Mensal:</b> R$ {{$finCob['valor_mensal']}}<br>
                        <b>Numero de Beneficiarios:</b> {{count($finCob['lead_produto_agregado']['lead_produto_agregado_itens'])}}<br>
                        <b>Numero de Parcelas:</b> {{$finCob['vezes_cobranca']}}<br>
                    </div>

                </div><div class="display_none btnSE" style="margin-top: 56px; text-align: center"><button id="atualizaLead" class="btn btn-success">Salvar Edições</button></div>
            </form>
            <div class="row tos">
                <div class="col s12" style="margin-top: 30px; font-weight: bolder; text-align: center;"><h3>Termos de uso</h3></div>
            </div>
            <div class="row">
                <div class="txt_info" style="margin-bottom: 40px;">
                    <p>Mussum Ipsum, cacilds vidis litro abertis. Nullam volutpat risus nec leo commodo, ut interdum diam laoreet. Sed non consequat odio. Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Posuere libero varius. Nullam a nisl ut ante blandit hendrerit. Aenean sit amet nisi. in elementis mé pra quem é amistosis quis leo.</p>

                    <p>Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Sapien in monti palavris qui num significa nadis i pareci latim. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
                    <p>Praesent vel viverra nisi. Mauris aliquet nunc non turpis scelerisque, eget. Sapien in monti palavris qui num significa nadis i pareci latim. Não sou faixa preta cumpadi, sou preto inteiris, inteiris. Si u mundo tá muito paradis? Toma um mé que o mundo vai girarzis!</p>
                </div>

                <div class="col s4 txt_la">
                        <?php $i=1; ?>
                    <form id="formUpdateLeadAgregado">
                        @foreach($finCob['lead_produto_agregado']['lead_produto_agregado_itens'] as $leadAgregado)
                        <div class="las">
                            <div class="block_code_{{$leadAgregado['lead_agregado']['lead_agregado_id']}}" style="">
                                <div class="">
                                    <label for="lead_agregado_nome">Nome do Beneficiario {{$i}}</label>
                                    <input  @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Nome do beneficiario" id="lead_agregado_nome_{{$i}}" name="lead_agregado_nome" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_nome']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_nome">Nome da Mãe {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Mae do beneficiario" id="lead_agregado_mae_{{$i}}" name="lead_agregado_mae" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_mae']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_nascimento">Data de Nascimento {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Data de nascimento" id="lead_agregado_nascimento_{{$i}}" class="data_nasc" name="lead_agregado_nascimento" type="text" data-nascimento="{{$leadAgregado['lead_agregado']['lead_agregado_nascimento']}}" value="{{ date('d/m/Y',strtotime($leadAgregado['lead_agregado']['lead_agregado_nascimento'])) }}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_cep">CEP do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Cep do Beneficiario" id="lead_agregado_cep_{{$i}}" name="lead_agregado_cep" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_cep']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_uf">Estado do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Estado do Beneficiario" id="lead_agregado_uf_{{$i}}" name="lead_agregado_uf" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_uf']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_cidade">Cidade do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Cidade do Beneficiario" id="lead_agregado_cidade_{{$i}}" name="lead_agregado_cidade" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_cidade']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_bairro">Bairro do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Bairro do Beneficiario" id="lead_agregado_bairro_{{$i}}" name="lead_agregado_bairro" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_bairro']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_logradouro">Logradouro do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif placeholder="Logradouro do Beneficiario" id="lead_agregado_logradouro_{{$i}}" name="lead_agregado_logradouro" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_logradouro']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_complemento">Complemento do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0)  @else disabled @endif placeholder="Complemento do Beneficiario" id="lead_agregado_complemento_{{$i}}" name="lead_agregado_complemento" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_complemento']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_numero">Numero do Beneficiario {{$i}}</label>
                                    <input @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif  placeholder="Numero de endereco do Beneficiario " id="lead_agregado_numero_{{$i}}" name="lead_agregado_numero" type="text"  value="{{$leadAgregado['lead_agregado']['lead_agregado_numero']}}">
                                </div>
                                <div class="">
                                    <label for="lead_agregado_sexo">Sexo do Beneficiario {{$i}}</label>
                                    <div>
                                        <select name="lead_agregado_sexo_{{$i}}" id="lead_agregado_sexo_{{$i}}" @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0) required @else disabled @endif>
                                            <option value="m" @if($leadAgregado['lead_agregado']['lead_agregado_sexo']=="m") selected @else {{""}} @endif>Masculino</option>
                                            <option value="f" @if($leadAgregado['lead_agregado']['lead_agregado_sexo']=="f") selected @else {{""}} @endif>Femenino</option>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" class="lead_agregado_id" id="lead_agregado_id_{{$i}}" name="lead_agregado_id_{{$i}}" value="{{$leadAgregado['lead_agregado']['lead_agregado_id']}}">
                                <input type="hidden" name="financeiro_contratacao_id" value="{{$finCob['financeiro_contratacao_id']}}" id="financeiro_contratacao_id_{{$i}}">
                            </div>
                        </div>
                        <?php $i++; ?>
                        <br>
                        @endforeach
                        <div style="text-align: center">
                            @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0)<input type="submit" class="btn btn-success" id="editCad" value="Editar Cadastro">@endif
                            @if($finCob['termo_cobranca']['termo_cobranca_termo_ok']==0)<a href="#" id="okTermo" data-contratacao_id="{{$finCob['financeiro_contratacao_id']}}" class="btn btn-success">OK, confirmo dados e efetivar pagamento</a> @endif
                            <button class="display_none btn btn-success" id="viewBoleto">Ver Boleto</button>
                        </div>
                    </form>
                    <div style="text-align: center">
                        <a href="#" class="btn btn-default">Clique aqui para ver o Contrato</a>
                        <a href="#" class="btn btn-default">Clique aqui para ver as Carencias</a>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <img class="alignnone size-full wp-image-223" src="https://agentebrasil.com.br/wp-content/uploads/2015/09/AGENTE-BRASIL-LOGO-AZUL-BG.gif" alt="logo Agente Brasil">
            <p>Florianópolis, SC, Brasil</p>
            <p>Email:sac@agentebrasil.com.br</p>
            <p>Fone: 0800 649 4040</p>
        </footer>
        <script>
            var host = "<?=getenv("SERVICO_CLIENT"); ?>";
            //editar ao lado de cada informação do lead
            $('#edita_info').on('click',function(){
                $('#lead_nome').attr("disabled",false);
                $('#lead_email').attr("disabled",false);
                $('#lead_cpf').attr("disabled",false);
                $('#lead_mae').attr("disabled",false);
                $('#lead_uf').attr("disabled",false);
                $('#lead_cidade').attr("disabled",false);
                $('#lead_bairro').attr("disabled",false);
                $('#lead_logradouro').attr("disabled",false);
                $('#lead_cep').attr("disabled",false);
                $('#lead_nascimento').attr("disabled",false);
                $('#lead_fone').attr("disabled",false);
                $('.display_none').removeClass("display_none");
            });
            //
            var getJsonObjectLead = function(){
                 jsonObjUpdateLead = {
                    'lead_nome':$('#lead_nome').val(),
                    'lead_email':$('#lead_email').val(),
                    'lead_mae':$('#lead_mae').val(),
                    'lead_uf':$('#lead_uf').val(),
                    'lead_cidade':$('#lead_cidade').val(),
                    'lead_bairro':$('#lead_bairro').val(),
                    'lead_logradouro':$('#lead_logradouro').val(),
                    'lead_cep': $('#lead_cep').val(),
                    'lead_fone':$('#lead_fone').val(),
                    'lead_nascimento':$('#lead_nascimento').val(),
                    'financeiro_contratacao_id':$('#financeiro_contratacao_id').val(),
                    'lead_id':$('#lead_id').val()
                };
                return jsonObjUpdateLead;
            };

            var getJsonObjectLeadAgregado = function(){
                count = $('.lead_agregado_id').length;
                var objectLeadAgregado = [];
                for(i=1;i<=count;i++){
                    objectLeadAgregado.push({
                        'lead_agregado_nome':$('#lead_agregado_nome_'+i).val(),
                        'lead_agregado_mae':$('#lead_agregado_mae_'+i).val(),
                        'lead_agregado_nascimento':$('#lead_agregado_nascimento_'+i).val(),
                        'lead_agregado_cep':$('#lead_agregado_cep_'+i).val(),
                        'lead_agregado_uf':$('#lead_agregado_uf_'+i).val(),
                        'lead_agregado_cidade':$('#lead_agregado_cidade_'+i).val(),
                        'lead_agregado_bairro':$('#lead_agregado_bairro_'+i).val(),
                        'lead_agregado_logradouro':$('#lead_agregado_logradouro_'+i).val(),
                        'lead_agregado_complemento':$('#lead_agregado_complemento_'+i).val(),
                        'lead_agregado_numero':$('#lead_agregado_numero_'+i).val(),
                        'lead_agregado_sexo':$('#lead_agregado_sexo_'+i).val(),
                        'lead_agregado_id':$('#lead_agregado_id_'+i).val(),
                        'financeiro_contratacao_id':$('#financeiro_contratacao_id').val()
                    });
                }
                return objectLeadAgregado;
            };

            var disabledLeadAgregado = function(){
                $('#lead_nome').attr('disabled',true),
                $('#lead_email').attr('disabled',true),
                $('#lead_mae').attr('disabled',true),
                $('#lead_uf').attr('disabled',true),
                $('#lead_cidade').attr('disabled',true),
                $('#lead_bairro').attr('disabled',true),
                $('#lead_logradouro').attr('disabled',true),
                $('#lead_cep').attr('disabled',true),
                $('#lead_fone').attr('disabled',true),
                $('#lead_nascimento').attr('disabled',true)
            };

            $("#formLeadUpdate").on('submit',function(e){
                e.preventDefault();
                $('#formLeadUpdate').validate({
                    rules: {
                        lead_nome: { lettersonly: true },
                        lead_mae: {lettersonly: true},
                        lead_uf: { lettersonly: true },
                        lead_estado: { lettersonly: true },
                        lead_cidade: { lettersonly: true },
                        //lead_bairro: { lettersonly: true },
                        lead_cep: { cepvalid: true }
                    },
                    submitHandler: function(form){
                        $.blockUI({'message':'Processando...'});
                        console.log(getJsonObjectLead());
                        var promise = $.ajax({'method':'post','url':host+"/client/termos-de-aceite/update-lead",'data':getJsonObjectLead()});
                        promise.done(function(resp){
                            disabledLeadAgregado();
                            $.unblockUI();
                            alert('Atualizado com sucesso');
                            $('.btnSE').addClass('display_none');
                        });
                    }
                });
            });

            $("#formUpdateLeadAgregado").on('submit',function(e){
                e.preventDefault();
                $('#formUpdateLeadAgregado').validate({
                    rules: {
                        lead_agregado_nome : { lettersonly: true},
                        lead_agregado_mae: { lettersonly: true},
                        lead_agregado_cep: { cepvalid: true},
                        lead_agregado_uf: { lettersonly: true},
                        lead_agregado_cidade: { lettersonly: true},
                        //lead_agregado_bairro: {lettersonly: true},
                        //lead_agregado_logradouro: {lettersonly: true},
                        //lead_agregado_complemento: {lettersonly: true},
                        lead_agregado_numero: { onlynumbers:true}
                    },
                    submitHandler: function(form){
                        $.blockUI({'message':'Processando...'});
                        var data = JSON.stringify(getJsonObjectLeadAgregado());
                        console.log(data);
                        var promise = $.ajax({'method':'post','url':host+"/client/termos-de-aceite/update-lead-agregado",'headers':{'Content-Type':'appplication/json'},'data': data});
                        promise.done(function(resp){
                            $.unblockUI();
                            alert(resp.message);
                        });
                    }
                });
            });

            $("#okTermo").on("click",function (e) {
                e.preventDefault();
                $.blockUI({'message':'Registrando Confirmação...'});
                contratacao_id = $(this).data('contratacao_id');
                var promise = $.ajax({'method':'get','url':host+'/client/termos-de-aceite/confirmacao/'+contratacao_id});
                promise.done(function(response){
                   $.unblockUI();
                    if(response.type=="boleto"){
                        alert('Informações confirmadas com sucesso!');
                        alert('Foi enviado um boleto para o seu email\nVocê também pode visualizá-lo abaixo');
                        $('#viewBoleto').removeClass('display_none');
                        window.localStorage.setItem('url_boleto',response.data.file);
                        $('#okTermo').remove();
                        $('#editCad').remove();
                        $('input').attr('disabled',true);
                        $('select').attr('disabled',true);
                    }
                    if(response.type=="cartao_credito"){
                        if(response.data.status=="denied"){
                            txt = confirm("Seu cartão foi recusado, deseja alterar a forma de pagamento para boleto ?");
                            if(txt===true) {
                                updateFormaPagamento(contratacao_id);
                            }
                        } else if(response.data.status=="pago"){
                            alert('Informações confirmadas com sucesso!');
                            alert('Pagamento confirmado com sucesso');
                            $('input').attr('disabled',true);
                            $('select').attr('disabled',true);
                            $('#okTermo').remove();
                            $('#editCad').remove();
                        }
                    }
                    //location.reload();
                });

                promise.fail(function(){
                    $.unblockUI();
                    alert("Erro ao confirmar informações!");
                });
            });

            var updateFormaPagamento = function(contratacao_id){
                $.unblockUI();
                $.blockUI({'message':'Alterando forma de pagamento para boleto...'});
                var promiseUpdate = $.ajax({'method':'get','url':host+'/client/termos-de-aceite/atualiza-forma-pagamento/'+contratacao_id});
                promiseUpdate.done(function(response){
                    $.unblockUI();
                    if(response.type=="boleto"){
                        alert('Informações confirmadas com sucesso!');
                        alert('Foi enviado um boleto para o seu email\nVocê também pode visualizá-lo abaixo');
                        $('#viewBoleto').removeClass('display_none');
                        window.localStorage.setItem('url_boleto',response.data.file);
                        $('#okTermo').remove();
                        $('#editCad').remove();
                        $('input').attr('disabled',true);
                        $('select').attr('disabled',true);
                    }
                });
                promiseUpdate.fail(function(response){
                    $.unblockUI();
                    alert("Erro ao confirmar informações!");
                });
            };

            $('#viewBoleto').on('click',function(e){
                e.preventDefault();
                window.open(window.localStorage.getItem('url_boleto'),'_blank');
            });

            $('.data_nasc').mask('00/00/0000');

            jQuery.validator.addMethod("cepvalid", function(value, element) {
                return /^[0-9]/.test(value) && value.length===8;
            }, "CEP invalido");

            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return /^[a-zA-Z-ãâáàÃÂÁÀçÇñÑíîÍÎéêèÉÊÈõóòôÓÒÔûúùÛÚÙ\s]*$/.test(value);
            }, "Somente Letras");

            jQuery.validator.addMethod("onlynumbers", function(value, element) {
                return /^[0-9]/.test(value);
            }, "Somente Numeros");
        </script>
    </body>     
</html>