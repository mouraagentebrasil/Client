<?php

namespace Modules\Client\Console;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Modules\Client\Entities\FinanceiroCobranca;
use Modules\Client\Entities\FinanceiroFormaPagamento;
use Modules\Client\Entities\LogFinanceiroCobranca;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class VerificaContrato extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'verifica-contrato';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';
    protected $client;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $cobrancas = FinanceiroCobranca::with(['FinanceiroContratacao'])
            ->selectRaw("foi_pago, count(*) as num, financeiro_contratacao_id, data_vencimento as data_primeiro_pagamento, gateway_slug")
            ->groupBy('financeiro_contratacao_id')
            ->havingRaw('num = 1 and foi_pago = 1')
            ->get()->toArray();
        $time_b = time();
        //levando em consideração que o contrato tem sempre um ano de duração minima
        foreach ($cobrancas as $cobranca){
            echo "Primeiro Pagamento: ".$cobranca['data_primeiro_pagamento']." Financeiro Contratacao: ".$cobranca['financeiro_contratacao_id']." Numero de Parcelas: ".($cobranca['financeiro_contratacao']['vezes_cobranca']-1)."\n";
            $exp = explode("-",$cobranca['data_primeiro_pagamento']);
            $mes = $exp[1];
            $ano = $exp[0];
            for($i=1;$i<=($cobranca['financeiro_contratacao']['vezes_cobranca']-1);$i++){
                if($mes==12) {$mes = 1;$ano++;}
                else $mes++;
                $data_inclusao = date('Y-m-d H:i:s');
                $vencimento_recorrente = $ano."-".str_pad($mes,2,0,STR_PAD_LEFT)."-".str_pad($cobranca['financeiro_contratacao']["dia_recorrencia"],2,0,STR_PAD_LEFT);
                $time_init = time();

                $financeiro_cobranca_id = FinanceiroCobranca::create([
                    'gateway_slug'=>$cobranca['gateway_slug'],
                    'data_inclusao'=>$data_inclusao,
                    'data_vencimento'=>$vencimento_recorrente,
                    'financeiro_contratacao_id'=>$cobranca['financeiro_contratacao']['financeiro_contratacao_id'],
                    'financeiro_forma_pagamento_id'=>$cobranca['financeiro_contratacao']['financeiro_forma_pagamento_id'],
                    'valor'=>$cobranca['financeiro_contratacao']['valor_mensal'],
                    'usuario_id'=>1,
                ])->financeiro_cobranca_id;

                $obj_pag = '';
                if($cobranca['financeiro_contratacao']['financeiro_forma_pagamento_id']==1){
                    echo "\n gerar uma cobranca via boleto financeiro_cobranca->".$financeiro_cobranca_id."\n";
                    $obj_pag = $this->generateBoleto($financeiro_cobranca_id);
                }

                LogFinanceiroCobranca::create([
                    'data_insercao'=>$data_inclusao,
                    'cron_type'=>'VerificaContrato',
                    'hits_cobranca'=>1,
                    'data_last_operation'=>$data_inclusao,
                    'time_operation'=>time()-$time_init,
                    'obj_ws'=>json_encode($obj_pag),
                    'financeiro_cobranca_id'=>$financeiro_cobranca_id
                ]);
            }
        }
        echo "\n".(time()-$time_b)."s de duração \n";
        return 0;
    }

    public function generateBoleto($financeiro_cobranca_id)
    {
        sleep(1); //delay mup
        try {
            $request = $this->client->request('GET',getenv('SERVICO_PAGAMENTO').'/pagamentos/generateBoletoFinanceiroAB/'.$financeiro_cobranca_id);
            $data = json_decode($request->getBody()->getContents());
            $cobranca_id = $data->cobranca;
            FinanceiroCobranca::where('financeiro_cobranca_id',$financeiro_cobranca_id)->update([
                'gateway_current_code'=>$cobranca_id
            ]);
            return $data;
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }

    //de 10 as 23:00 (as lote2), de 23:01 a 09:59 (lote1)
    public function processLoteGrafica(){
        $date = date('Y-m-d H:i:s');
    }
}
