<?php

namespace Modules\Client\Console;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Client\Entities\Lead;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DomCrawler\Crawler;

class GeratxtBoletos extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generatetxtBoletos';
    protected $client;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $data = DB::connection('mysql2')->table('cobranca')
            ->where('forma_pagamento','boleto')
            ->where('link','<>','')
            ->where('linha_digitavel','<>','')
            ->where('fl_importado',0)
            ->get();
        $cobranca = [];
        foreach ($data as $cob){
            try {
                $boleto = file_get_contents('http://agentebrasil.com/fab/boletos/'.$cob->cobranca_id.'.html');
                $crawler = new Crawler($boleto);
                $nosso_numero = $crawler->filter('.right.textRight')->eq(3)->text();
                if($cob->nosso_numero!=NULL) {
                    $cobranca[$cob->cobranca_id] = $cob->nosso_numero;
                    echo $cob->cobranca_id." OK".PHP_EOL;
                    continue;
                }
                DB::beginTransaction();
                DB::connection('mysql2')->table('cobranca')
                    ->where('cobranca_id',$cob->cobranca_id)
                    ->update([
                        'nosso_numero'=>$nosso_numero,
                        //'fl_importado'=>1
                    ]);
                DB::commit();
                $cobranca[$cob->cobranca_id] = $cob->nosso_numero;
                echo "Cobranca ATUALIZADA, nosso numero: ".$nosso_numero.PHP_EOL;
            } catch (\Exception $e){
                DB::rollback();
                echo "Erro (".$cob->cobranca_id."): ".$e->getMessage().PHP_EOL;
            }
        }

        echo PHP_EOL."----------------------------------------------------------------------".PHP_EOL;
        echo PHP_EOL."--------------------------- GERANDO TXT ------------------------------".PHP_EOL;
        $file =  time()."_".date('Y-m-d').".txt";
        try {
            DB::beginTransaction();
            $importacao_grafica_id = DB::connection('mysql2')->table('importacao_grafica')->insertGetId([
                'data_importacao'=>date('Y-m-d H:i:s'),
                'arquivo_importacao'=>storage_path('boletos_txt/'.$file)
            ]);

            foreach ($data as $cob){
                $time_init = time();
                $lead = $this->getInfoLead($cob->lead_id);
                if(empty($lead)) continue;
                if(!isset($cobranca[$cob->cobranca_id])) continue;
                $findCob = DB::connection('mysql2')->table('importacao_grafica_cobranca')->where('cobranca_id',$cob->cobranca_id)->first();
                if(!is_null($findCob)) {
                    echo "Cobranca (".$cob->cobranca_id."): Já foi importado ".PHP_EOL;
                    continue;
                }
                $template_txt = $this->getTxt();
                $linha = str_replace("[LINHA_DIGITAVEL]",$this->formatLinhaDigitavel($cob->linha_digitavel),$template_txt);
                $linha = str_replace("[DATA_VENCIMENTO]",date("d/m/Y",strtotime($cob->data_vencimento)),$linha);
                $linha = str_replace("[DATA_PROCESSAMENTO]",date("d/m/Y",strtotime($cob->data_inclusao)),$linha);
                $linha = str_replace("[COBRANCA_ID]",$cob->cobranca_id,$linha);
                $linha = str_replace("[NOSSO_NUMERO]",$cobranca[$cob->cobranca_id],$linha);
                $linha = str_replace("[VALOR]",$cob->valor_pagamento,$linha);
                $linha = str_replace("[INTRUCOES]",$cob->instrucoes,$linha);
                $linha = str_replace("[LEAD_NOME]",$lead[0]->lead_nome,$linha);
                $linha = str_replace("[LEAD_LOGRADOURO]",strtoupper($lead[0]->lead_logradouro),$linha);
                $linha = str_replace("[LEAD_CEP]",$lead[0]->lead_cep,$linha);
                $linha = str_replace("[LEAD_CIDADE]",strtoupper($lead[0]->lead_cidade),$linha);
                $linha = str_replace("[LEAD_UF]",strtoupper($lead[0]->lead_uf),$linha);
                $linha = str_replace("[LEAD_CPF]",$lead[0]->lead_cpf,$linha);

                file_put_contents(storage_path('boletos_txt/'.$file),$linha.PHP_EOL,FILE_APPEND);
                DB::connection('mysql2')->table('importacao_grafica_cobranca')->insert([
                    'cobranca_id'=>$cob->cobranca_id,
                    'tempo_processamento'=>time()-$time_init,
                    'importacao_grafica_id'=>$importacao_grafica_id
                ]);

                DB::connection('mysql2')->table('cobranca')
                    ->where('cobranca_id',$cob->cobranca_id)
                    ->update([
                        'fl_importado'=>1
                    ]);
                echo $linha.PHP_EOL;
                echo PHP_EOL."-----------------------------------------------------------------------".PHP_EOL;
            }
            DB::commit();
        } catch (\Exception $e){
            DB::rollback();
            echo $e->getMessage().PHP_EOL;
        }
        return 0;
    }

    public function getTxt(){
        return "237-2|[LINHA_DIGITAVEL]|Banco Bradesco S.A. Pagável preferencialmente em qualquer Agência Bradesco|[DATA_VENCIMENTO]|AGENTE BRASIL CORRETORA DE SEGUROS E BENEFICIOS LTDA 22.627.199/0001-15 Av. Trompowsky, 291 sala 1001 torre 2|3.308-1/0.027.000-8|[DATA_PROCESSAMENTO]|[COBRANCA_ID]|Outro|N|[DATA_PROCESSAMENTO]|[NOSSO_NUMERO]||[COBRANCA_ID]|26|Real|||R$ [VALOR]|[INTRUCOES]|(-) Desconto/Abatimento (-) Outras deduções (+) Mora/Multa (+) Outros Acréscimos (+) Valor cobrado|[LEAD_NOME] [LEAD_LOGRADOURO] [LEAD_CEP] [LEAD_CIDADE] [LEAD_UF] [LEAD_CPF]";
    }

    public function getInfoLead($lead_id){
        $data = Lead::select(['lead_nome','lead_cpf','lead_logradouro','lead_cep','lead_cidade','lead_uf'])->where('lead_id',$lead_id)->get();
        return $data;
    }

    public function formatLinhaDigitavel($linha_digitavel){
        $s1 = substr($linha_digitavel,0,5);
        $s2 = substr($linha_digitavel,5,5);
        $s3 = substr($linha_digitavel,10,5);
        $s4 = substr($linha_digitavel,15,6);
        $s5 = substr($linha_digitavel,21,5);
        $s6 = substr($linha_digitavel,26,6);
        $s7 = substr($linha_digitavel,32,1);
        $s8 = substr($linha_digitavel,33,47);
        return $s1.".".$s2.".".$s3.".".$s4.".".$s5.".".$s6.".".$s7.".".$s8;
    }
}