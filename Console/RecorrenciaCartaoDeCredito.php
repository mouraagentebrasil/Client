<?php

namespace Modules\Client\Console;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Modules\Client\Entities\FinanceiroCobranca;
use Modules\Client\Entities\LogFinanceiroCobranca;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class RecorrenciaCartaoDeCredito extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'r-cartao-credito';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';
    protected $client;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //todos os cartões que foram possiveis de cobrar
        $getCobrancaCartao = FinanceiroCobranca::
                with([
                    'FinanceiroContratacao'=>function($query) {
                        $query->with(['LeadProdutoAgregado'=>function($query){
                            $query->with(['Lead' => function ($query) {
                                $query->select(['lead_id','lead_nome'])->with('CartaoCredito');
                            }]);
                        }]);
                    }
                ])
                ->where('data_vencimento','<=',date('Y-m-d'))
                ->where('foi_pago',0)
                ->where('financeiro_forma_pagamento_id',2)
                ->whereIn('financeiro_cobranca_status',['avencer','pendente'])
                ->get()->toArray();
        foreach ($getCobrancaCartao as $cobranca){
            $time_init = time();
            $objCartao = $cobranca['financeiro_contratacao']['lead_produto_agregado']['lead']['cartao_credito'][0];

            $obj_pag = $this->generateCardCreditPayment(
                $objCartao['lead_card_numero'],
                $objCartao['lead_card_titular_nome'],
                $objCartao['lead_card_mes']."/".$objCartao['lead_card_ano'],
                $objCartao['lead_card_bandeira'],
                $objCartao['lead_card_cvv'],
                $cobranca['financeiro_cobranca_id']
            );

            LogFinanceiroCobranca::firstOrCreate([
                'data_insercao'=>date('Y-m-d H:i:s'),
                'cron_type'=>'RecorrenciaCartaoDeCredito',
                'hits_cobranca'=>1,
                'data_last_operation'=>date('Y-m-d H:i:s'),
                'time_operation'=>time()-$time_init,
                'obj_ws'=>json_encode($obj_pag),
                'financeiro_cobranca_id'=>$cobranca['financeiro_cobranca_id']
            ]);
        }
        return 0;
    }

    public function generateCardCreditPayment($card_number,$holder,$expiration_data,$brand,$cvv,$financeiro_cobranca_id){
        try {
            sleep(2);
            $data = [
                'financeiro_cobranca_id'=>$financeiro_cobranca_id,
                'Payment'=>[
                    'CreditCard'=>[
                        'CardNumber'=>$card_number,
                        'Holder'=>$holder,
                        'ExpirationDate'=>$expiration_data,
                        'Brand'=>$brand,
                        'SecurityCode'=>$cvv
                    ]
                ]
            ];
            //var_dump($data); exit;
            $url = getenv('SERVICO_PAGAMENTO')."/pagamentos/generateCobCardCredit";
            $r = $this->client->request('POST',$url,[
                'form_params'=>$data
            ]);
            return $r->getBody()->getContents();
        } catch (\Exception $e){
            return $e->getMessage();
        }
    }

}
