<?php

namespace Modules\Client\Repositories;

use Modules\Client\Entities\LogRedecredenciada;
use Illuminate\Support\Facades\DB;

class LogRedecredenciadaRepository
{
    
    //retorna id(number) ou array caso algo de errado
    public function store($data,$exeception = null){
        $message = null;
        $model = new LogRedecredenciada();
        $validate = $model->validate($data,$exeception);
        if(is_array($validate)) return $validate;
        try {
            DB::beginTransaction();
            $data = $model->create($data);
            DB::commit();
            return $data->log_redecredenciada_id;
        } catch(\Exception $e) {
            DB::rollback();
            return ['message'=>$e->getMessage()];
        }
    }

    //retorna id(number) ou array caso algo de errado
    public function update($id,$data,$exeception = null){
        $message = null;
        $model = new LogRedecredenciada();
        $validate = $model->validate($data,$exeception);
        if(is_array($validate)) return $validate;
        try {
            DB::beginTransaction();
            $data = $model->where('log_redecredenciada_id',$id)->update($data);
            DB::commit();
            return true;
        } catch(\Exception $e) {
            DB::rollback();
            return ['message'=>$e->getMessage()];
        }
    }

    //retorna true ou array caso algo de errado
    public function delete($id){
        $message = null;
        $model = new LogRedecredenciada();
        if(is_null($id)) return ['message'=>'id não pode ser nulo'];
        try {
            DB::beginTransaction();
            $data = $model->where('log_redecredenciada_id',$id)->delete($id);
            DB::commit();
            return true;
        } catch(\Exception $e) {
            DB::rollback();
            return ['message'=>$e->getMessage()];
        }
    }

    //retorna o builder de um model
    public function builder()
    {
        $model = new LogRedecredenciada();
        return $model;
    }
}