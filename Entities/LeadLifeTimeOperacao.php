<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LeadLifeTimeOperacao extends Model
{
    protected $table='lead_life_time_operacao';
    protected $primaryKey='lead_life_time_operacao_id';
    protected $fillable=['lead_life_time_operacao_id','lead_life_time_operacao_name','lead_life_time_operacao_txt_template'];
    public $timestamps = false;

    

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
