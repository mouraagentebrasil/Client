<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class Grupo extends Model
{
    protected $table='grupo';
    protected $primaryKey='grupo_id';
    protected $fillable=['grupo_id','grupo_nome','grupo_criacao'];
    public $timestamps = false;

    public function GrupoRotaB(){
        return $this->belongsToMany('\Modules\Client\Entities\GrupoRota','grupo_rota','grupo_id','rota_id');
    }

    public function GrupoRota(){
        return $this->hasMany('\Modules\Client\Entities\GrupoRota');
    }

    public function GrupoRotaFrontend(){
        return $this->hasMany('\Modules\Client\Entities\GrupoRotaFrontend');
    }

    public function GrupoRotaFrontendB(){
        return $this->belongsToMany('\Modules\Client\Entities\GrupoRotaFrontend','grupo_rota_frontend','grupo_id','rota_frontend_id');
    }
    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
