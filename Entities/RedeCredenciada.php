<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class RedeCredenciada extends Model
{
    protected $table='rede_credenciada';
    protected $primaryKey='rede_credenciada_id';
    protected $fillable=['rede_credenciada_id','data_importacao','cep_inicial','cep_final','uf','cidade','seguradora_id','fl_ativo','codeimport'];
    public $timestamps = false;

    public function Seguradora()
    {
        return $this->belongsTo('\Modules\Client\Entities\Seguradora');
    }

    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
