<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class GrupoRota extends Model
{
    protected $table='grupo_rota';
    protected $primaryKey='grupo_rota_id';
    protected $fillable=['grupo_rota_id','rota_id','grupo_id','permitido'];
    public $timestamps = false;

    

    public function Rota()
    {
        return $this->belongsTo('\Modules\Client\Entities\Rota');
    }

    public function Grupo()
    {
        return $this->belongsTo('\Modules\Client\Entities\Grupo');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
