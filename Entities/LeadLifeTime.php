<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LeadLifeTime extends Model
{
    protected $table='lead_life_time';
    protected $primaryKey='lead_life_time_id';
    protected $fillable=['lead_life_time_id','lead_id','data_operacao','lead_life_time_retorno','valor_agregado','usuario_id','lead_life_time_operacao_id'];
    public $timestamps = false;

    

    public function Lead()
    {
        return $this->belongsTo('\Modules\Client\Entities\Lead');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    public function OperacaoTimeLifeLead()
    {
        return $this->belongsTo('\Modules\Client\Entities\LeadLifeTimeOperacao','lead_life_time_operacao_id');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
