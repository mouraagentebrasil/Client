<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class FinanceiroContratacao extends Model
{
    protected $table='financeiro_contratacao';
    protected $primaryKey='financeiro_contratacao_id';
    protected $fillable=[
        'financeiro_contratacao_id','data_contratacao','valor_total',
        'valor_mensal','vezes_cobranca','lead_produto_agregados_id',
        'usuario_id','financeiro_forma_pagamento_id',
        'dia_recorrencia','fl_importacao','data_importacao','no_remessa','fl_ativo'];
    public $timestamps = false;



    public function LeadProdutoAgregado()
    {
        return $this->belongsTo('\Modules\Client\Entities\LeadProdutoAgregado','lead_produto_agregados_id','lead_produto_agregados_id');
    }

    public function FinanceiroCobranca()
    {
        return $this->hasMany('\Modules\Client\Entities\FinanceiroCobranca');
    }

    public function TermoCobranca()
    {
        return $this->hasOne('\Modules\Client\Entities\TermoCobranca');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }


    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
                continue;
            }
            if(!isset($data[$fillable[$i]])){
                $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
