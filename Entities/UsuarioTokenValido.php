<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class UsuarioTokenValido extends Model
{
    protected $table='usuario_token_valido';
    protected $primaryKey='usuario_token_valido_id';
    protected $fillable=['usuario_token_valido_id','token_valido','is_valid','date_request','usuario_id'];
    public $timestamps = false;

    

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
