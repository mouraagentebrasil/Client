<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LeadProdutoAgregado extends Model
{
    protected $table='lead_produto_agregado';
    protected $primaryKey='lead_produto_agregados_id';
    protected $fillable=['lead_produto_agregados_id','produto_id','usuario_id','lead_id'];
    public $timestamps = false;

    

    public function Produto()
    {
        return $this->hasOne('\Modules\Client\Entities\Produto','produto_id','produto_id');
    }

    public function FinanceiroContratacao()
    {
        return $this->hasMany('\Modules\Client\Entities\FinanceiroContratacao','lead_produto_agregados_id','lead_produto_agregados_id');
    }

    public function LeadProdutoAgregadoItens(){
        return $this->hasMany('\Modules\Client\Entities\LeadProdutoAgregadoItens','lead_produto_agregados_id','lead_produto_agregados_id');
    }

    public function Lead(){
        return $this->belongsTo('\Modules\Client\Entities\Lead','lead_id');
    }

    public function Carteira()
    {
        return $this->belongsTo('\Modules\Client\Entities\Carteira');
    }
    //O usuario nesse caso é um vendedor
    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
