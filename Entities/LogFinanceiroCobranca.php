<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LogFinanceiroCobranca extends Model
{
    protected $table='log_financeiro_cobranca';
    protected $primaryKey='log_financeiro_cobranca_id';
    protected $fillable=[
        'log_financeiro_cobranca_id','data_insercao','data_operacao',
        'cron_type','hits_cobranca','data_last_operation',
        'time_operation','obj_ws','financeiro_cobranca_id'];
    public $timestamps = false;

    

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
