<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table='produto';
    protected $primaryKey='produto_id';
    protected $fillable=['produto_id','produto_nome','usuario_id','seguradora_id','produto_criacao','produto_status','produto_descricao','produto_valor','produto_parcelas','produto_vigencia_inicio','produto_vigencia_fim','fl_ativo','legado_importacao'];
    public $timestamps = false;

    

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    public function Seguradora()
    {
        return $this->belongsTo('\Modules\Client\Entities\Seguradora');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
