<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LogUsuarioLogin extends Model
{
    protected $table='log_usuario_login';
    protected $primaryKey='log_login_id';
    protected $fillable=['log_login_id','usuario_id','data_login'];
    public $timestamps = false;

    

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
