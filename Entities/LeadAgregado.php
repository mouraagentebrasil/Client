<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LeadAgregado extends Model
{
    protected $table='lead_agregado';
    protected $primaryKey='lead_agregado_id';
    protected $fillable=[
        'lead_agregado_id','lead_agregado_tipo','lead_agregado_nome',
        'lead_agregado_cpf','lead_agregado_nascimento','lead_agregado_mae',
        'lead_agregado_email','lead_agregado_fone','lead_agregado_cep',
        'lead_agregado_uf','lead_agregado_cidade','lead_agregado_bairro',
        'lead_agregado_logradouro','lead_agregado_complemento','lead_agregado_numero',
        'lead_agregado_sexo','lead_agregado_criacao','fl_ativo','lead_id','lead_agregado_parentesco',
        'lead_agregado_civil','lead_agregado_meta_id'
    ];
    public $timestamps = false;

    public function Lead()
    {
        return $this->belongsTo('\Modules\Client\Entities\Lead');
    }

    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
