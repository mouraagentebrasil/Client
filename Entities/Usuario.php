<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table='usuario';
    protected $primaryKey='usuario_id';
    protected $fillable=['usuario_id','login','password','email','usuario_criacao','usuario_telefone','fl_ativo','fl_lead'];
    public $timestamps = false;

    public function GrupoUsuario(){
        return $this->hasOne('\Modules\Client\Entities\GrupoUsuario');
    }

    public function GrupoUsuarioB(){
        return $this->belongsToMany('\Modules\Client\Entities\GrupoUsuario','grupo_usuario','usuario_id','grupo_id');
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = sha1($value);
    }
    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
