<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    protected $table='lead';
    protected $primaryKey = 'lead_id';
    protected $fillable=[
        'lead_id','lead_email','lead_nome','lead_cobrado','lead_fone',
        'lead_nascimento','lead_cpf','lead_ip','lead_cep','lead_uf',
        'lead_bairro','lead_logradouro','lead_cidade','lead_numero','lead_complemento',
        'lead_estado_civil','lead_criacao','lead_geracao','lead_capturado',
        'lead_cobranca','lead_saldo','lead_sexo','campanha_id','usuario_id',
        'lead_mae','lead_custo','lead_legado_importacao','lead_legado_id','lead_has_exported','lead_tag','lead_hits_interest',
        'lead_code_import','lead_object_ayty','lead_ayty_num_call','lead_ayty_num_call_finalizado'
    ];

    public $timestamps = false;



    public function Campanha()
    {
        return $this->belongsTo('\Modules\Client\Entities\Campanha');
    }

    public function LeadAgregado()
    {
        return $this->hasMany('\Modules\Client\Entities\LeadAgregado');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    public function CartaoCredito(){
        return $this->hasMany('\Modules\Client\Entities\LeadCard');
    }

    public function LeadProdutoAgregado() {
        return $this->hasMany('\Modules\Client\Entities\LeadProdutoAgregado');
    }

    public function validate($data,$execeptions)
    {
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];
        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
                continue;
            }

            if(!isset($data[$fillable[$i]])){
                $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
