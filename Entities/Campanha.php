<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class Campanha extends Model
{
    protected $table='campanha';
    protected $primaryKey='campanha_id';
    protected $fillable=['campanha_id','campanha_nome','campanha_url','produto_id','usuario_id','valor_investido'];
    public $timestamps = false;

    

    public function Produto()
    {
        return $this->belongsTo('\Modules\Client\Entities\Produto');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
