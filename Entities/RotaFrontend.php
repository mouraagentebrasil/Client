<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class RotaFrontend extends Model
{
    protected $table='rota_frontend';
    protected $primaryKey='rota_frontend_id';
    protected $fillable=['rota_frontend_id','rota_frontend_url','rota_frontend_icon','rota_frontend_nome'];
    public $timestamps = false;

    

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
