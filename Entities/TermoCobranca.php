<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class TermoCobranca extends Model
{
    protected $table='termo_cobranca';
    protected $primaryKey='termo_cobranca_id';
    protected $fillable=['termo_cobranca_id','termo_cobranca_token','financeiro_contratacao_id','termo_cobranca_is_valid','number_hits','termo_cobranca_termo_ok','termo_cobranca_termo_ok_date','termo_cobranca_last_hit'];
    public $timestamps = false;

    

    public function ContratacaoFinanceiro()
    {
        return $this->belongsTo('\Modules\Client\Entities\ContratacaoFinanceiro');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
