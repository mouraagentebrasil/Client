<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class FinanceiroCobranca extends Model
{
    protected $table='financeiro_cobranca';
    protected $primaryKey='financeiro_cobranca_id';
    protected $fillable=[
        'financeiro_cobranca_id','gateway_slug','data_inclusao','data_vencimento',
        'foi_pago','valor','financeiro_contratacao_id','financeiro_forma_pagamento_id',
        'usuario_id','valor_pago','gateway_current_code','data_pago','financeiro_cobranca_status',
        'fl_primeira_parcela'
    ];
    public $timestamps = false;


    public function LeadProdutoAgregado()
    {
        return $this->belongsTo('\Modules\Client\Entities\LeadProdutoAgregado','lead_produto_agregados_id','lead_produto_agregados_id');
    }

    public function FinanceiroContratacao()
    {
        return $this->belongsTo('\Modules\Client\Entities\FinanceiroContratacao');
    }

    public function PagamentoFormaFinanceiro()
    {
        return $this->belongsTo('\Modules\Client\Entities\PagamentoFormaFinanceiro');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
