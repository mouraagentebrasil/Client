<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class UsuarioResetPassword extends Model
{
    protected $table='usuario_reset_password';
    protected $primaryKey='usuario_reset_password_id';
    protected $fillable=['usuario_reset_password_id','usuario_reset_token','usuario_reset_password_requisicao','usuario_id'];
    public $timestamps = false;

    

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
