<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LeadProdutoAgregadoItens extends Model
{
    protected $table='lead_produto_agregado_itens';
    protected $primaryKey='lead_produto_agregado_itens_id';
    protected $fillable=['lead_produto_agregado_itens_id','lead_produto_agregados_id','lead_agregado_id','carteira_id'];
    public $timestamps = false;

    

    public function LeadProdutoAgregado()
    {
        return $this->belongsTo('\Modules\Client\Entities\LeadProdutoAgregado');
    }

    public function LeadAgregado()
    {
        return $this->belongsTo('\Modules\Client\Entities\LeadAgregado');
    }

    public function Carteira()
    {
        return $this->belongsTo('\Modules\Client\Entities\Carteira');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
