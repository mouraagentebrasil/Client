<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class GrupoRotaFrontend extends Model
{
    protected $table='grupo_rota_frontend';
    protected $primaryKey='grupo_rota_fronted_id';
    protected $fillable=['grupo_rota_fronted_id','grupo_id','rota_frontend_id'];
    public $timestamps = false;

    

    public function Grupo()
    {
        return $this->belongsTo('\Modules\Client\Entities\Grupo');
    }

    public function RotaFrontend()
    {
        return $this->hasOne('\Modules\Client\Entities\RotaFrontend','rota_frontend_id','rota_frontend_id');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
