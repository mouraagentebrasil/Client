<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class LeadCard extends Model
{
    protected $table='lead_card';
    protected $primaryKey='lead_card_id';
    protected $fillable=['lead_card_id','lead_card_titular_nome','lead_card_numero','lead_card_cvv','lead_card_mes','lead_card_ano','lead_card_bandeira','lead_id'];
    public $timestamps = false;

    

    public function Lead()
    {
        return $this->belongsTo('\Modules\Client\Entities\Lead');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
