<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class OperacoesCarteira extends Model
{
    protected $table='operacoes_carteira';
    protected $primaryKey='operacoes_carteira_id';
    protected $fillable=['operacoes_carteira_id','lead_id','produto_id','usuario_id','carteira_id','operacao','operacoes_carteira_date','operacoes_carteira_date_operacao'];
    public $timestamps = false;

    

    public function Lead()
    {
        return $this->belongsTo('\Modules\Client\Entities\Lead');
    }

    public function Produto()
    {
        return $this->belongsTo('\Modules\Client\Entities\Produto');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    public function Carteira()
    {
        return $this->belongsTo('\Modules\Client\Entities\Carteira');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
