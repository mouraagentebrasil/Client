<?php

namespace Modules\Client\Entities;

use Illuminate\Database\Eloquent\Model;

class Carteira extends Model
{
    protected $table='carteira';
    protected $primaryKey='carteira_id';
    protected $fillable=['carteira_id','fl_ativo','carteira_criacao','carteira_number','seguradora_id','usuario_id','lead_id','lead_agregado_id','carteira_obj_retorno_ws'];
    public $timestamps = false;

    

    public function Seguradora()
    {
        return $this->belongsTo('\Modules\Client\Entities\Seguradora');
    }

    public function Usuario()
    {
        return $this->belongsTo('\Modules\Client\Entities\Usuario');
    }

    public function Lead()
    {
        return $this->belongsTo('\Modules\Client\Entities\Lead');
    }

    public function LeadAgregado()
    {
        return $this->belongsTo('\Modules\Client\Entities\LeadAgregado');
    }

    
    public function validate($data,$execeptions)
    {
        //para não validar nenhum campo basta passar "*" como execeptions
        if($execeptions=="*") return true;
        $fillable = $this->fillable;
        unset($fillable[0]);
        sort($fillable);
        $message = [];

        for($i=0;$i<count($fillable);$i++){
            if($execeptions != null && in_array($fillable[$i],$execeptions)){
               continue;
            }
            if(!isset($data[$fillable[$i]])){
                 $message[] = "preencha o campo ".$fillable[$i];
            } elseif($data[$fillable[$i]]==""){
                $message[] = "o campo ".$fillable[$i]." está nulo";
            }
        }
        if(empty($message)) return true;
        return ['message'=>$message];
    }
}
